include { GFF; GTF; NUCLEOTIDE; BUSCO; AGAT; LOG } from '../modules/contam_removal.nf'

workflow REMOVE_CONTAM {

    take:
    gtf
    contam
    protein
    genome
    entap
    
    main:
    if(params.contam != null ){
        GFF         ( contam, protein, gtf, params.prefix )
        GTF         ( GFF.out.no_contam_gff, params.prefix )
        NUCLEOTIDE  ( genome, GFF.out.no_contam_gff, params.prefix )
        BUSCO       ( GFF.out.no_contam_pep, params.busco_lineage )
        AGAT        ( GFF.out.no_contam_gff, params.prefix )
        LOG         ( BUSCO.out.contam_txt, AGAT.out.contam_stats, entap, params.contam )

        merge = Channel.empty()
            merge
                .concat( LOG.out.contam_log )
                .set { no_contam }
    }
    else if(params.contam == null ){
        no_contam = Channel.empty()
    }

    emit:
    no_contam
}
