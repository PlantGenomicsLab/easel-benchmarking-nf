include { MIKADO; BUSCO; AGAT; ENTAP; ADD_FUNCTION; LOG } from '../modules/metrics.nf'

workflow SUMMARY_STATS {
  
    take:
    unfiltered_gff
    filtered_gff
    longest_gff
    primary_gff
    filtered_protein
    longest_protein
    primary_protein
    unfiltered_protein
    odb
    entap_config
    reference
    reference_db
    
    main:

    merge = Channel.empty()
    merge
        .concat( filtered_gff, unfiltered_gff, longest_gff, primary_gff )
        .flatten()
        .map { file -> tuple(file.baseName, file) }
        .view()
        .set { annotation }

    if(params.reference != null){
        MIKADO ( reference, annotation, params.prefix )
    } 

    merge
        .concat( filtered_protein, unfiltered_protein, longest_protein, primary_protein )
        .flatten()
        .map { file -> tuple(file.baseName, file) }
        .view()
        .set { pep }

    pep
        .combine( entap_config ).combine( reference_db )
        .view()
        .set { entap_tuple }

    BUSCO                   ( pep, odb )
    AGAT                    ( annotation, params.prefix )
    ENTAP                   ( entap_tuple )

    annotation
        .combine( ENTAP.out.entap_annotations )
        .view()
        .set { entap_annotation }

    ADD_FUNCTION            ( entap_annotation )

    busco_txt = BUSCO.out.busco_txt
    agat_stats = AGAT.out.stats
    entap_log = ENTAP.out.entap_log
    entap_no_contam_filtered = ENTAP.out.entap_log_filtered
    entap_contam = ENTAP.out.entap_contam


    busco_txt
        .join(agat_stats, by:0).join(entap_log)
        .view()
        .set { entap }

    LOG                     ( entap )
    metrics_log = LOG.out.log.collect()

    emit:
    metrics_log
    entap_contam
    entap_no_contam_filtered
    

}
