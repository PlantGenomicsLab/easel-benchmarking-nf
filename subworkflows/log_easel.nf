include { EASEL } from '../modules/log.nf'

workflow OUTPUT {

    take:
    genome
    rna_reads
    alignments
    assembly
    annotation
    contam
	
    main:
    if (params.contam == null){
        if (params.resume_filtering == true ){
        EASEL    ( genome, rna_reads, alignments, [], annotation, [])
        }
        else if (params.resume_filtering == false ){
        EASEL    ( genome, rna_reads, alignments, assembly, annotation, [])
        }
    }
    else if(params.contam != null){
        if (params.resume_filtering == true ){
        EASEL    ( genome, rna_reads, alignments, [], annotation, contam)
        }
        else if (params.resume_filtering == false ){
        EASEL    ( genome, rna_reads, alignments, assembly, annotation, contam)
        }
    }	
}
