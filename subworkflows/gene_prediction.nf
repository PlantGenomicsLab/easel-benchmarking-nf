include { PSICLASS; STRINGTIE2; MERGE_STRINGTIE2; FASTA; ORF; EGGNOG; PREDICT; GENEMODEL; CLUSTER; GET_COMPLETE } from '../modules/gene_prediction/gene_model.nf'
include { EST_ALIGN; EST_HINTS; PROTEIN_ALIGN; PROTEIN_HINTS; ORTHODB_HINTS; TRAINING_SET; AUGUSTUS_EST; GFFREAD; AUGUSTUS_PROTEIN; AUGUSTUS_ORTHODB; COMBINE_EST; COMBINE_PROTEIN; COMBINE_ORTHODB; COMBINE_RNA; GRAB_TRANSCRIPTS; COMPARE_RNA; PREDICTIONS; FIX_EXONS; RENAME_ATTRIBUTES; COMBINE_ALL; GENE_OVERLAP; COMPLETE_GENEMODEL; FORMAT_GFF; LIST } from '../modules/gene_prediction/prediction.nf'

workflow ANNOTATION {
    take:
    bam
	hisat_alignments
	genome
	split
	configEST
	configProtein
	eggnog_db
	gmap
	orthodb

    main:
    /////////////////////////////////////////////////////
    //gene_model.nf - assemble and filter gene model   // 
	/////////////////////////////////////////////////////

	if(params.resume_filtering == false){ 

		bam_ch = bam.toList()
    	PSICLASS 					( bam_ch )
    	psiclass_gtf = PSICLASS.out.gtf
		STRINGTIE2	                ( hisat_alignments, params.gap_tolerance )
		merge_ch = STRINGTIE2.out.stringtie2_gtf.collect()
		MERGE_STRINGTIE2            ( merge_ch )
		stringtie2_gtf = MERGE_STRINGTIE2.out.merged_gtf

		merge = Channel.empty()
		merge
			.concat(stringtie2_gtf).concat(psiclass_gtf)
			.flatten()
			.map { file -> tuple(file.baseName, file) }
			.view()
			.set { assemblies }

		FASTA			            ( genome, assemblies )
		fasta = FASTA.out.fasta
		gff3 = FASTA.out.gff3
		assembly_log = FASTA.out.log.collect()

    	ORF          	   			( FASTA.out.fasta )
    	directory = ORF.out.LongOrfsDirFiles
    	EGGNOG                      ( ORF.out.longOrfsPepFiles, eggnog_db )
    	blast = EGGNOG.out.eggnogBlastp

		fasta
        	.join(blast, by:0).join(directory)
			.view()
        	.set { transdecoder }

    	PREDICT 	                ( transdecoder )
    	orf = PREDICT.out.transdecoderGFF3
   
		orf
        	.join(gff3, by:0).join(fasta)
			.view()
        	.set { model }

		GENEMODEL                   ( model )
		CLUSTER 	                ( PREDICT.out.transdecoderCDS, params.cluster_id )
		centroids = CLUSTER.out.centroids
		gff3_model = GENEMODEL.out.genomeGFF3

		centroids
        	.join(gff3_model, by:0)
			.view()
        	.set { complete }

    	GET_COMPLETE 	            ( complete )
    	gff3_complete = GET_COMPLETE.out.GeneModel

    //////////////////////////////////////////////////
    //prediction.nf - hint generation and augustus // 
	/////////////////////////////////////////////////
    	EST_ALIGN         			( PREDICT.out.transdecoderCDS, gmap, genome )
		EST_HINTS 	                ( EST_ALIGN.out.align )
		PROTEIN_ALIGN			    ( genome, PREDICT.out.transdecoderPEP, params.miniprot_args )
		miniprot = PROTEIN_ALIGN.out.miniprot
		PROTEIN_HINTS         	    ( miniprot, genome )

		GFFREAD					    ( GET_COMPLETE.out.GeneModel )
		gtf = GFFREAD.out.gtf

		gtf
        	.join(gff3_complete, by:0)
			.view()
        	.set { train }
        
		TRAINING_SET		            ( train, genome, params.prefix, params.test_count, params.train_count, params.kfold, params.rounds, params.augustus )
		training_set = TRAINING_SET.out.output
		est = EST_HINTS.out.est.join(training_set, by: 0)
		protein = PROTEIN_HINTS.out.protein.join(training_set, by: 0)

		if(params.external_protein == true){
			orthodb_miniprot = orthodb.map { file -> tuple(file.baseName, file) }
			ORTHODB_HINTS         	    ( orthodb_miniprot, genome )

			orthodb_protein_hints = ORTHODB_HINTS.out.protein
			orthodb_protein_hints
        		.map { file -> tuple('stringtie2', file) }
        		.join(training_set, by: 0)
        		.view()
        		.set { stringtie_orthodb }

    		orthodb_protein_hints
        		.map { file -> tuple('psiclass', file) }
        		.join(training_set, by: 0)
        		.view()
        		.set { psiclass_orthodb }

    		merge
				.concat( psiclass_orthodb, stringtie_orthodb )
				.transpose()
				.view()
				.set { orthodb_protein }
		}

    	genome_ch = split.flatten().map { file -> tuple(file.baseName, file) }
    	genome_ch
    		.combine(est)
    		.view()
        	.set { est_hints }

		AUGUSTUS_EST 	            ( configEST, est_hints, params.prefix, params.augustus )

		genome_ch
    		.combine(protein)
    		.view()
        	.set { protein_hints }

		AUGUSTUS_PROTEIN            ( configProtein, protein_hints, params.prefix, params.augustus )



		if(params.external_protein == true){

			genome_ch
    			.combine(orthodb_protein)
    			.view()
        		.set { orthodb_hints }

			AUGUSTUS_ORTHODB 			( configProtein, orthodb_hints, params.prefix, params.augustus )
		}


		est_ch = AUGUSTUS_EST.out.estHints.collect()
		COMBINE_EST                 ( est_ch )
		protein_ch = AUGUSTUS_PROTEIN.out.proteinHints.collect()
		COMBINE_PROTEIN             ( protein_ch )

		merge
			.concat( GENEMODEL.out.stringtie2_model, GENEMODEL.out.psiclass_model )
			.flatten()
			.collect()
			.view()
			.set { rna_gff }

		COMBINE_RNA					( rna_gff )
		COMPARE_RNA					( COMBINE_RNA.out.transdecoder )
		GRAB_TRANSCRIPTS			( COMBINE_RNA.out.transdecoder, COMPARE_RNA.out.gffcompare )

		if(params.external_protein == true){
			orthodb_ch = AUGUSTUS_ORTHODB.out.orthodb_proteinHints.collect()
			COMBINE_ORTHODB				( orthodb_ch )

			merge
				.concat( GRAB_TRANSCRIPTS.out.rna, COMBINE_PROTEIN.out.protein_stringtie2, COMBINE_EST.out.est_stringtie2, COMBINE_PROTEIN.out.protein_psiclass, COMBINE_EST.out.est_psiclass, COMBINE_ORTHODB.out.orthodb_psiclass, COMBINE_ORTHODB.out.orthodb_stringtie2 )
				.flatten()
				.map { file -> tuple(file.baseName, file) }
				.view()
				.set { gff }
			merge
				.concat( COMBINE_PROTEIN.out.protein_stringtie2, COMBINE_EST.out.est_stringtie2, COMBINE_PROTEIN.out.protein_psiclass, COMBINE_EST.out.est_psiclass, COMBINE_ORTHODB.out.orthodb_psiclass, COMBINE_ORTHODB.out.orthodb_stringtie2, GENEMODEL.out.stringtie2_model, GENEMODEL.out.psiclass_model )
				.flatten()
				.collect()
				.view()
				.set { list }
		}
		else if(params.external_protein == false){
			merge
				.concat( GRAB_TRANSCRIPTS.out.rna, COMBINE_PROTEIN.out.protein_stringtie2, COMBINE_EST.out.est_stringtie2, COMBINE_PROTEIN.out.protein_psiclass, COMBINE_EST.out.est_psiclass )
				.flatten()
				.map { file -> tuple(file.baseName, file) }
				.view()
				.set { gff }
			merge
				.concat( COMBINE_PROTEIN.out.protein_stringtie2, COMBINE_EST.out.est_stringtie2, COMBINE_PROTEIN.out.protein_psiclass, COMBINE_EST.out.est_psiclass, GENEMODEL.out.stringtie2_model, GENEMODEL.out.psiclass_model )
				.flatten()
				.collect()
				.view()
				.set { list }
		}
		PREDICTIONS					( list )		
		FIX_EXONS                   ( gff, genome )
		RENAME_ATTRIBUTES           ( FIX_EXONS.out.fixed )
		COMBINE_ALL                 ( RENAME_ATTRIBUTES.out.rename.collect() )
		GENE_OVERLAP				( COMBINE_ALL.out.combined )
        COMPLETE_GENEMODEL                      ( GENE_OVERLAP.out.overlap_unfiltered, genome )       
		FORMAT_GFF					( COMPLETE_GENEMODEL.out.start, genome, params.prefix )
		format_gff = FORMAT_GFF.out.unfiltered_fixed
		//I merged RNA together so support is both together
		LIST  						( RENAME_ATTRIBUTES.out.rename.collect() )
	}
	else if(params.resume_filtering == true){
		
		augustus_ch = Channel.fromPath(params.augustus_list).splitCsv().flatten()
			.map { gff -> file(gff) }
			.view()
			.set { augustus_gff }

		rna_ch = Channel.fromPath(params.rna_list).splitCsv().flatten()
			.map { gff -> file(gff) }
			.collect()
			.view()
			.set { rna_gff }

		COMBINE_RNA					( rna_gff )
		COMPARE_RNA					( COMBINE_RNA.out.transdecoder )
		GRAB_TRANSCRIPTS			( COMBINE_RNA.out.transdecoder, COMPARE_RNA.out.gffcompare )
		compare_rna = GRAB_TRANSCRIPTS.out.rna

		merge = Channel.empty()
		merge
			.concat( augustus_gff, compare_rna )
			.flatten()
			.map{ file -> tuple(file.baseName, file)}
			.view()
			.set { gff }

		assembly_log = Channel.empty()
		FIX_EXONS                   ( gff, genome )
		RENAME_ATTRIBUTES           ( FIX_EXONS.out.fixed )
		COMBINE_ALL                 ( RENAME_ATTRIBUTES.out.rename.collect() )
		GENE_OVERLAP				( COMBINE_ALL.out.combined )
        COMPLETE_GENEMODEL                      ( GENE_OVERLAP.out.overlap_unfiltered, genome )       
        FORMAT_GFF                                      ( COMPLETE_GENEMODEL.out.start, genome, params.prefix )		
		format_gff = FORMAT_GFF.out.unfiltered_fixed
		LIST  						( RENAME_ATTRIBUTES.out.rename.collect() )
	}
    emit:
	FORMAT_GFF.out
	LIST.out
	assembly_log
	format_gff
}
