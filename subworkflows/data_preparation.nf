include { PERCENT_MASKED; SPLIT; FOLD } from '../modules/data_preparation/genome.nf'
include { FETCH_RNA; FASTP; QC_REMOVE_SAMPLES; QC_LOG } from '../modules/data_preparation/rna_reads.nf'
include { ORTHODB_ALIGN; GMAP_INDEX; HISAT2_INDEX; HISAT2_ALIGN; ALIGNMENT_REMOVE_SAMPLES; MAPPING_RATE; ALIGNMENT_LOG } from '../modules/data_preparation/alignments.nf'
include { REFERENCE; ENTAP; ENTAP_CONFIG; ORTHODB } from '../modules/data_preparation/databases.nf'


workflow DATA_INPUT {

    take:
    genome
	
    main:
    merge = Channel.empty()
    if(params.reference != null ){
        REFERENCE     ( params.reference, params.prefix )
        reference = REFERENCE.out.gtf
    }
    else if(params.reference == null ){
        reference = Channel.empty()
    }

    ENTAP               ( params.config_ini )
    ENTAP_CONFIG        ( params.taxon ?: '', params.tcoverage, params.qcoverage, ENTAP.out.entap_db, ENTAP.out.eggnog_db, ENTAP.out.data_eggnog, params.contam ?: '' )

    if(params.reference_db == null ){
        reference_db = ENTAP.out.entap_reference
    }
    else if(params.reference_db != null ){
        reference_db = Channel.fromPath(params.reference_db)
    }

    ORTHODB             ( params.order, params.busco_lineage, params.taxid ?:'', params.user_protein )
    ORTHODB_ALIGN       ( genome, ORTHODB.out.protein, params.miniprot_args )
    PERCENT_MASKED      ( genome )
	SPLIT        		( genome, params.bins )
    FOLD                ( genome, params.prefix )

    if(params.resume_filtering == false){ 
        GMAP_INDEX          ( genome )
        gmap_index = GMAP_INDEX.out.gmapindex
    
        if(params.bam == null){
            if(params.sra != null && params.user_reads != null){
                sra_ch = Channel.fromPath(params.sra).splitCsv().flatten()
                FETCH_RNA           ( sra_ch )
                fetch_rna = FETCH_RNA.out.sra
                read_pairs_ch = Channel.fromFilePairs(params.user_reads, checkIfExists: true)
                fetch_rna
                    .concat( read_pairs_ch )
                    .view()
                    .set { rna }
            }
            else if(params.sra != null && params.user_reads == null){
                sra_ch = Channel.fromPath(params.sra).splitCsv().flatten()
                FETCH_RNA           ( sra_ch )
                fetch_rna = FETCH_RNA.out.sra
                fetch_rna
                    .set { rna }
            }
            else if(params.sra == null && params.user_reads != null){
                read_pairs_ch = Channel.fromFilePairs(params.user_reads, checkIfExists: true)
                read_pairs_ch
                    .set { rna }
            }
            FASTP               ( rna, params.fastp_args )
            QC_REMOVE_SAMPLES   ( FASTP.out.trimmed_json, params.total_reads, params.mean_length )
            QC_LOG              ( QC_REMOVE_SAMPLES.out.qc.collect(), params.total_reads, params.mean_length )
            HISAT2_INDEX        ( genome )
            HISAT2_ALIGN        ( QC_REMOVE_SAMPLES.out.pass_fastq, HISAT2_INDEX.out.hisat2index, params.hisat2_min_intronlen, params.hisat2_max_intronlen, params.hisat2_args )
            hisat2 = HISAT2_ALIGN.out.sam_tuple
            ALIGNMENT_REMOVE_SAMPLES    ( hisat2, params.rate )
        }

        if(params.bam != null ){
            ch_bam = Channel.fromPath(params.bam).flatten().map { file -> tuple(file.baseName, file) }
            MAPPING_RATE                ( ch_bam )
            bam_map = MAPPING_RATE.out.bam_user
        
            if(params.sra == null && params.user_reads == null){
                ALIGNMENT_REMOVE_SAMPLES    ( bam_map, params.rate )
                QC_LOG                      ( [], params.total_reads, params.mean_length )
            }
            else if(params.sra != null && params.user_reads == null){
                sra_ch = Channel.fromPath(params.sra).splitCsv().flatten()
                FETCH_RNA           ( sra_ch )
                fetch_rna = FETCH_RNA.out.sra
                fetch_rna
                    .set { rna }
                FASTP               ( rna, params.fastp_args )
                QC_REMOVE_SAMPLES   ( FASTP.out.trimmed_json, params.total_reads, params.mean_length )
                QC_LOG              ( QC_REMOVE_SAMPLES.out.qc.collect(), params.total_reads, params.mean_length )
                HISAT2_INDEX        ( genome )
                HISAT2_ALIGN        ( QC_REMOVE_SAMPLES.out.pass_fastq, HISAT2_INDEX.out.hisat2index, params.hisat2_min_intronlen, params.hisat2_max_intronlen, params.hisat2_args )
                hisat2 = HISAT2_ALIGN.out.sam_tuple
                bam_map
                    .concat( hisat2 )
                    .flatten()
                    .view()
                    .set { combined }
                ALIGNMENT_REMOVE_SAMPLES    ( combined, params.rate )
            }
            else if(params.sra == null && params.user_reads != null){
                read_pairs_ch = Channel.fromFilePairs(params.user_reads, checkIfExists: true)
                read_pairs_ch
                    .set { rna }
                FASTP               ( rna, params.fastp_args )
                QC_REMOVE_SAMPLES   ( FASTP.out.trimmed_json, params.total_reads, params.mean_length )
                QC_LOG              ( QC_REMOVE_SAMPLES.out.qc.collect(), params.total_reads, params.mean_length )
                HISAT2_INDEX        ( genome )
                HISAT2_ALIGN        ( QC_REMOVE_SAMPLES.out.pass_fastq, HISAT2_INDEX.out.hisat2index, params.hisat2_min_intronlen, params.hisat2_max_intronlen, params.hisat2_args )
                hisat2 = HISAT2_ALIGN.out.sam_tuple
                bam_map
                    .concat( hisat2 )
                    .view()
                    .set { combined }
                ALIGNMENT_REMOVE_SAMPLES    ( combined, params.rate )
            }
            else if(params.sra != null && params.user_reads != null){
                sra_ch = Channel.fromPath(params.sra).splitCsv().flatten()
                FETCH_RNA           ( sra_ch )
                fetch_rna = FETCH_RNA.out.sra
                read_pairs_ch = Channel.fromFilePairs(params.user_reads, checkIfExists: true)
                fetch_rna
                    .concat( read_pairs_ch )
                    .view()
                    .set { rna }
                FASTP               ( rna, params.fastp_args )
                QC_REMOVE_SAMPLES   ( FASTP.out.trimmed_json, params.total_reads, params.mean_length )
                QC_LOG              ( QC_REMOVE_SAMPLES.out.qc.collect(), params.total_reads, params.mean_length )
                HISAT2_INDEX        ( genome )
                HISAT2_ALIGN        ( QC_REMOVE_SAMPLES.out.pass_fastq, HISAT2_INDEX.out.hisat2index, params.hisat2_min_intronlen, params.hisat2_max_intronlen, params.hisat2_args )
                hisat2 = HISAT2_ALIGN.out.sam_tuple
                bam_map
                    .concat( hisat2 )
                    .transpose()
                    .view()
                    .set { combined }
                ALIGNMENT_REMOVE_SAMPLES    ( combined, params.rate )
            }
        }
    }
    if(params.resume_filtering == true){
        ch_bam = Channel.fromPath(params.bam).flatten().map { file -> tuple(file.baseName, file) }
        MAPPING_RATE                ( ch_bam )
        bam_map = MAPPING_RATE.out.bam_user
        ALIGNMENT_REMOVE_SAMPLES    ( bam_map, params.rate )
        QC_LOG                      ( [], params.total_reads, params.mean_length )
        gmap_index = Channel.empty()
    }

    ALIGNMENT_LOG              ( ALIGNMENT_REMOVE_SAMPLES.out.mr.collect(), params.rate )

    emit:
    ALIGNMENT_REMOVE_SAMPLES.out.pass_bam_tuple
    ALIGNMENT_REMOVE_SAMPLES.out.pass_bam
    SPLIT.out
    PERCENT_MASKED.out
    QC_LOG.out
    ALIGNMENT_LOG.out
    ENTAP.out.data_eggnog
    ENTAP.out.entap_db
    ENTAP.out.eggnog_db
    ORTHODB_ALIGN.out
    FOLD.out
    gmap_index
    reference
    ENTAP_CONFIG.out
    reference_db
}
