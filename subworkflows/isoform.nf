include { LONGEST_ISOFORM; F1_ISOFORM } from '../modules/primary_gene.nf'

workflow PRIMARY_GENE {

    take:
    filtered
    regressor
    
    main:

    if(params.primary_isoform == true ){
    LONGEST_ISOFORM         ( filtered, params.prefix )
    merge = Channel.empty()
    merge
        .concat( LONGEST_ISOFORM.out.filtered_gff )
        .set { longest_isoform }

    F1_ISOFORM         ( filtered, regressor, params.prefix )
    merge
        .concat( F1_ISOFORM.out.filtered_gtf )
        .set { primary_isoform }

    }
    else if(params.primary_isoform == false ){

    longest_isoform = Channel.empty()
    primary_isoform = Channel.empty()

    }
    
    emit:
    longest_isoform
    primary_isoform
}
