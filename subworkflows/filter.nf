include { UNFILTERED_GTF } from '../modules/feature_filtering/gff_to_gtf.nf'
include { FEATURE_COUNTS; GENE_EXPRESSION } from '../modules/feature_filtering/gene_expression.nf'
include { ORTHODB_BED; ORTHODB_FEATURE } from '../modules/feature_filtering/orthodb.nf'
include { SUPPORT } from '../modules/feature_filtering/support.nf'
include { MONO_MULTI } from '../modules/feature_filtering/multi_mono.nf' 
include { PROTEIN; NUCLEOTIDE; EGGNOG; EGGNOG_FEATURE } from '../modules/feature_filtering/gene_family.nf'  
include { START_STOP_CODONS } from '../modules/feature_filtering/full_length.nf' 
include { CDS_EXTRACT; CDS_REPEAT_CONTENT } from '../modules/feature_filtering/repeat_cds.nf' 
include { MAP } from '../modules/feature_filtering/map.nf' 
include { TRANSCRIPT_EXTRACT; LENGTH; MOLECULAR_WEIGHT; MERGE_TRANSCRIPT; GC; GC_PYTHON } from '../modules/feature_filtering/transcript.nf'
include { TRUE_START_SITE; PAD; SPLIT_FASTA; RNA_FOLD; MERGE; MATRIX } from '../modules/feature_filtering/folding.nf' 
include { REGRESSOR; CLASSIFIER; FILTERED_GTF } from '../modules/feature_filtering/random_forest.nf'

workflow FILTERING {

    take:
    genome
    eggnog_db
    bam
    orthodb_align
    gff
    list

    main:
    UNFILTERED_GTF          ( gff, params.prefix )
    FEATURE_COUNTS          ( bam, UNFILTERED_GTF.out.unfiltered_gtf )
    SUPPORT                 ( gff, list )
    ORTHODB_BED             ( orthodb_align, gff, params.fcoverage, params.Fcoverage )
    ORTHODB_FEATURE         ( SUPPORT.out.support, ORTHODB_BED.out.bed )
    GENE_EXPRESSION         ( FEATURE_COUNTS.out.counts.collect(), SUPPORT.out.support)
    MONO_MULTI              ( gff, SUPPORT.out.support )
    PROTEIN                 ( genome, UNFILTERED_GTF.out.unfiltered_gtf, params.prefix )
    protein = PROTEIN.out.protein
    NUCLEOTIDE              ( genome, UNFILTERED_GTF.out.unfiltered_gtf, params.prefix )
    cds = NUCLEOTIDE.out.cds
    EGGNOG                  ( PROTEIN.out.protein, eggnog_db )
    EGGNOG_FEATURE          ( SUPPORT.out.support, EGGNOG.out.mapper )
    START_STOP_CODONS       ( gff, SUPPORT.out.support )
    CDS_EXTRACT             ( genome, UNFILTERED_GTF.out.unfiltered_gtf, SUPPORT.out.support )
    CDS_REPEAT_CONTENT      ( CDS_EXTRACT.out.cds, SUPPORT.out.support )
    TRANSCRIPT_EXTRACT      ( genome, UNFILTERED_GTF.out.unfiltered_gtf, SUPPORT.out.support, params.parts )
    transcript_ch = TRANSCRIPT_EXTRACT.out.transcript_part.flatten().map { file -> tuple(file.baseName, file) }
    LENGTH                  ( transcript_ch )
    MOLECULAR_WEIGHT        ( transcript_ch )
    MERGE_TRANSCRIPT        ( MOLECULAR_WEIGHT.out.weight_txt.collect(), LENGTH.out.length_txt.collect(), SUPPORT.out.support)
    GC                      ( TRANSCRIPT_EXTRACT.out.transcript )
    GC_PYTHON               ( SUPPORT.out.support, GC.out.gc )
    TRUE_START_SITE         ( gff )
    PAD                     ( params.window, TRUE_START_SITE.out.text, genome )
    SPLIT_FASTA             ( PAD.out.fasta, params.parts )
    fasta_ch = SPLIT_FASTA.out.split.flatten().map { file -> tuple(file.baseName, file) }
    RNA_FOLD                ( fasta_ch )
    MERGE                   ( RNA_FOLD.out.rna_fold.collect() )
    MATRIX                  ( params.window, SUPPORT.out.support, MERGE.out.merge_rna )
    MAP                     ( MATRIX.out.gc_ratio.collect(), MATRIX.out.free_energy.collect(), GC_PYTHON.out.gc_track.collect(), MERGE_TRANSCRIPT.out.length.collect(), MERGE_TRANSCRIPT.out.weight.collect(), SUPPORT.out.support.collect(), MONO_MULTI.out.exonic.collect(), EGGNOG_FEATURE.out.eggnog.collect(), START_STOP_CODONS.out.start_codon.collect(), START_STOP_CODONS.out.stop_codon.collect(), CDS_REPEAT_CONTENT.out.cds_repeat.collect(), ORTHODB_FEATURE.out.orthodb.collect(), GENE_EXPRESSION.out.expression.collect())
    
    if(params.training_set == "plant"){
    REGRESSOR               ( MAP.out.features, params.plant )
    CLASSIFIER              ( MAP.out.features, params.plant )
    }
    else if(params.training_set == "vertebrate"){
    REGRESSOR               ( MAP.out.features, params.vertebrate )
    CLASSIFIER              ( MAP.out.features, params.vertebrate )
    }
    else if(params.training_set == "invertebrate"){
    REGRESSOR               ( MAP.out.features, params.invertebrate )
    CLASSIFIER              ( MAP.out.features, params.invertebrate )
    }
    FILTERED_GTF            ( CLASSIFIER.out.c_rf, REGRESSOR.out.r_rf, UNFILTERED_GTF.out.unfiltered_gtf, params.prefix, params.regressor )

    emit:
    FILTERED_GTF.out
    REGRESSOR.out
    protein
}

