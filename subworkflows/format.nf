include { GFF; NUCLEOTIDE; PROTEIN } from '../modules/final_predictions.nf'

workflow FINAL_PREDICTIONS {

    take:
    filtered
    longest
    primary
    genome
    
    main:

    merge = Channel.empty()

    if(params.primary_isoform == true ){
    merge
        .concat( filtered, primary )
        .flatten()
        .map { file -> tuple(file.baseName, file) }
        .view()
        .set { annotation }
    }
    else if(params.primary_isoform == false ){
    merge
        .concat( filtered )
        .flatten()
        .map { file -> tuple(file.baseName, file) }
        .view()
        .set { annotation }
    }

    GFF         ( annotation, params.prefix )
    filtered_gff = GFF.out.filtered_gff

    if(params.primary_isoform == true ){
    primary_gff = GFF.out.primary_gff
    }
    else if(params.primary_isoform == false){
    primary_gff = Channel.empty()
    }

    if(params.primary_isoform == true ){
    merge
        .concat( filtered, longest, primary )
        .flatten()
        .map { file -> tuple(file.baseName, file) }
        .view()
        .set { gtf }
    }
    else if(params.primary_isoform == false ){
    merge
        .concat( filtered )
        .flatten()
        .map { file -> tuple(file.baseName, file) }
        .view()
        .set { gtf }
    }

    NUCLEOTIDE  ( genome, gtf, params.prefix )
    PROTEIN     ( genome, gtf, params.prefix ) 

    filtered_protein = PROTEIN.out.filtered
 
    if(params.primary_isoform == true ){
    longest_protein = PROTEIN.out.longest
    primary_protein = PROTEIN.out.primary
    }
    else if(params.primary_isoform == false){
    longest_protein = Channel.empty()
    primary_protein = Channel.empty()
    }

    emit:
    filtered_gff
    primary_gff
    filtered_protein
    longest_protein
    primary_protein
}
