#!/bin/bash
#SBATCH --job-name=EASELv_1.3
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=10G

#If Nextflow and Singularity are not available as modules, be sure to follow dependency instructions
module load nextflow
module load singularity

#Singularity defaults to a scratch directory, I choose to redirect this into a new location with more storage
SINGULARITY_TMPDIR=$PWD/tmp
export SINGULARITY_TMPDIR

#The `xanadu` profile is specific to the UConn HPC, other available profiles can be found here: https://github.com/nf-core/configs
nextflow run main.nf -profile xanadu -params-file params.yaml -resume
