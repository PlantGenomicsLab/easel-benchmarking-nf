import sys
import re

if len(sys.argv) != 4:
    print("Usage: python script.py transcripts_file gff_file output_file")
    sys.exit(1)

transcripts_file = sys.argv[1]
gff_file = sys.argv[2]
output_file = sys.argv[3]

transcripts = set()

# Read patterns from transcripts file
with open(transcripts_file, 'r') as patterns_file:
    for line in patterns_file:
        transcripts.add(line.strip())

# Process gff file
with open(gff_file, 'r') as gff_file, open(output_file, 'w') as output_file:
    for line in gff_file:
        match_id = re.search(r'ID=([^;]+);', line)
        match_parent = re.search(r'Parent=([^;]+)', line)

        if match_id and match_id.group(1) in transcripts:
            output_file.write(line)
        elif match_parent and match_parent.group(1) in transcripts:
            output_file.write(line)
        elif 'Parent=' in line and re.search(r'Parent=([^;\s]+)', line).group(1) in transcripts:
            output_file.write(line)

