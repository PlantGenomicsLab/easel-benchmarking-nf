#!/usr/bin/env python

########################################################
#                                                      #
# add_percentage_masked_cds.py - calculates percentage #
# of masked bases in each CDS region                   #
#                                                      #
# Developed by Cynthia Webster (2022)                  #
# Plant Computational Genomics Lab                     #
# University of Connecticut                            #
#                                                      #
# This script is under the MIT License                 #
#                                                      #
########################################################


# Import necessary libraries
from Bio import SeqIO
import pandas as pd
import os
import sys
import re

# Get the filepath from command line argument
filepath = sys.argv[1]

# Parse the FASTA file and retrieve sequence objects
seq_objects = SeqIO.parse(filepath,'fasta')
sequences=[]

#load sequences into list container
for seq in seq_objects:
		sequences.append(seq)

seq_ids=[]
seq_lengths=[]
seq_count1=[]
seq_count2=[]
seq_masked=[]
seq_genes=[]

# Process each sequence
for record in sequences:
		seq_id=record.id
		seq_description=record.description
		# Extract gene information from the sequence description using regex
		fields=dict(re.findall(r'(\w+)=(.*?) ', seq_description))
		seq_gene=fields['gene']
		sequence=record.seq
		length=len(sequence)
		seq_genes.append(seq_gene)
		seq_ids.append(seq_id)
		seq_lengths.append(length)
		count1=0
		count2=0
		# Count lowercase and uppercase characters in the sequence
		for i in sequence:
			if(i.islower()):
				count1=count1+1
			elif(i.isupper()):
				count2=count2+1
		seq_count1.append(count1)
		seq_count2.append(count2)
		masked=(count1/length*100)
		seq_masked.append(masked)

# Create a DataFrame to store the sequence information
dataframe=pd.DataFrame()
dataframe['Transcript_ID']=seq_ids
dataframe['Gene_ID']=seq_genes
dataframe['Seq_Length']=seq_lengths
dataframe['Lowercase']=seq_count1
dataframe['Uppercase']=seq_count2
dataframe['Percent_Masked']=seq_masked

# Save the DataFrame as a tab-separated file
dataframe.to_csv('masked.tracking', sep='\t', encoding='utf-8', index=False)