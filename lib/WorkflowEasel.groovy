import nextflow.Nextflow

class WorkflowEasel {

    //
    // Check and validate parameters
    //
    public static void initialise(params, log, valid_params) {
        if (!params.genome) {
            Nextflow.error("Please provide an input FASTA file of softmasked sequences to the pipeline e.g. '--genome seqs.faa'")
        }
        if (!params.sra && !params.user_reads && !params.bam) {
            Nextflow.error("Please provide 'sra', 'user_reads' and/or 'bam' to the pipeline.")
        }
        if (!params.busco_lineage) {
            Nextflow.error("Please provide a relevant BUSCO protein database to the pipeline from bin/busco_lineage.txt e.g. '--busco_lineage embryophyta'")
        } else if (!valid_params['busco'].contains(params.busco_lineage)) {
            Nextflow.error("Invalid option: '${params.busco_lineage}'. Valid options for '--busco_lineage': ${valid_params['busco'].join(', ')}.")
        }
        if (!params.order) {
            Nextflow.error("Please provide a relevant taxonomic rank to filter OrthoDB proteins from bin/orthodb.txt e.g. '--order Brassicales'")
        } else if (!valid_params['orthodb'].contains(params.order)) {
            Nextflow.error("Invalid option: '${params.order}'. Valid options for '--order': ${valid_params['orthodb'].join(', ')}.")
        }
        if (!params.taxon) {
            Nextflow.error("Please provide a lineage such as species/kingdom/phylum to the pipeline for EnTAP filtering e.g. '--taxon arabidopsis'")
        }
        if (!params.training_set) {
            Nextflow.error("Please provide a training set '--training_set plant'")
        } else if (!valid_params['training'].contains(params.training_set)) {
            Nextflow.error("Invalid option: '${params.training_set}'. Valid options for '--training_set': ${valid_params['training'].join(', ')}.")
        }
    }
}
