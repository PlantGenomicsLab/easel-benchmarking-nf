process MIKADO {
   label 'process_medium'
   tag {id}
    publishDir "$params.outdir/metrics/mikado",  mode: 'copy'

    
    conda "bioconda::mikado"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/mikado:2.3.4--py39h919a90d_0' :
        'quay.io/biocontainers/mikado:2.3.4--py39h919a90d_0' }"
    
    input:
    path(reference)
    tuple val(id), path(prediction)
    val(mikado)
     
    output: 
    path("${id}*"), emit: prediction

      
    """
mikado compare -r ${reference} -p ${prediction} -pc -eu -erm -o ${id}

    """
}

process BUSCO {
    publishDir "$params.outdir/metrics/busco",  mode: 'copy', pattern: "*filtered*/*"

    tag {id}
    label 'process_medium'

    conda "bioconda::busco"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/busco:5.4.4--pyhdfd78af_0' :
        'quay.io/biocontainers/busco:5.4.4--pyhdfd78af_0' }"

    input:
    tuple val(id), path(protein)
    val(odb)

    output:
    tuple val(id), path("*filtered*/*"), emit: busco
    tuple val(id), path("*filtered*/*.txt"), emit: busco_txt
    tuple val(id), path("busco_complete.txt"), emit: busco_complete
      
    """
    busco -i ${protein} -l ${odb} -o ${id} -m Protein -c ${task.cpus}
    awk 'NR>3 { print \$3 }' *filtered*/*run_*/full_table.tsv > busco_complete.txt

    id=\$(echo "${id}" | sed 's/[^_]*_//')
    mv ${id} \$id
    """
}

process AGAT {
    publishDir "$params.outdir/metrics/agat",  mode: 'copy'
    label 'process_low'
    tag {id}

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.2.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.2.0--pl5321hdfd78af_0' }"

    input:
    tuple val(id), path(gff)
    val(species)

    output:
    tuple val(id), path("*.txt"), emit: stats
      
    """
    agat_sp_statistics.pl --gff ${gff} -o ${id}.txt
    """
}

process ENTAP {
    publishDir "$params.outdir/08_functional_annotation",  mode: 'copy', pattern: '*filtered*/*' 

    tag {id}
    label 'process_high'

    container "plantgenomics/easel:entap"
    
    input:
    tuple val(id), path(protein), path(config), path(database)

    output:
    tuple val(id), path("*filtered*/*"), emit: entap
    tuple val(id), path("*filtered*/log*"), emit: entap_log
    path("filtered/log*"), emit: entap_log_filtered, optional: true
    path("filtered/final_results/annotated_without_contam.faa"), emit: entap_no_contam_filtered, optional: true
    path("filtered/final_results/annotated_contam.faa"), emit: entap_contam, optional: true
    path("unfiltered/similarity_search/DIAMOND/overall_results/diamond_annotated_without_contam.tsv"), emit: entap_annotations, optional: true
      
    """
    id=\$(echo "${id}" | sed 's/[^_]*_//')
    entap_db=\$(readlink -f ${database}) 
    EnTAP --runP --ini ${config} -i ${protein} -d \$entap_db --threads ${task.cpus} 
    mv entap_outfiles \$id
    """
}
process ADD_FUNCTION {
    publishDir "$params.outdir/final_predictions",  mode: 'copy'
    label 'process_single'
    tag {id}

    input:
    tuple val(id), path(gff), path(annotation)

    output:
    tuple val(id), path("*functional.gff")

    """
    awk -F'\\t' 'BEGIN {OFS="\\t"} NR==FNR {annotations[\$1]=\$13; next} {match(\$9, /ID=([^;]+)/, arr); gene_id=arr[1]; if (gene_id in annotations) {\$9 = \$9 ";function=" annotations[gene_id]}}1' ${annotation} ${gff} > ${id}_functional.gff

    """
}
process LOG {
    label 'process_single'
    tag {id}
    publishDir "$params.outdir/log",  mode: 'copy' 
   
    input:
    tuple val(id), path(busco), path(agat), path(entap)
 
    output:
    path("log_${id}.txt"), emit: log
      
    script:
    """
    grep -m 1 "Number of gene" ${agat} > genes.txt
    grep -m 1 "Number of transcript" ${agat} > transcripts.txt
    grep "Total unique sequences with an alignment:" ${entap} > aligned.txt
    grep "The lineage" ${busco} > odb.txt
    grep "C:" ${busco} > busco.txt

    alignments=\$(sed 's/.*: //' aligned.txt | sed 's@^[^0-9]*\\([0-9]\\+\\).*@\\1@')
    odb=\$(grep -o '\\b\\w*\\_odb\\w*\\b' odb.txt)
    busco=\$(sed -e 's/[ \t]*//' busco.txt)
    genes=\$(grep -o '[0-9]\\+' genes.txt)
    transcripts=\$(grep -o '[0-9]\\+' transcripts.txt)

    # Check if "Number of single exon gene" exists before creating mono.txt
    if grep -q "Number of single exon gene" ${agat}; then
       grep -m 1 "Number of single exon gene" ${agat} > mono.txt
       mono=\$(grep -o '[0-9]\\+' mono.txt)
       multi=\$((\$genes - \$mono))
       mono_multi=\$(echo "scale=2 ; \$mono / \$multi" | bc)
    else
       mono_multi="0"  
    fi

    entap=\$(echo "scale=2 ; \$alignments / \$transcripts" | bc) 

    echo " " >> log_${id}.txt
    if [[ "${id}" == *_filtered ]]; then
    	echo "##### Filtered #####" >> log_${id}.txt
    elif [[ "${id}" == *_unfiltered* ]]; then
    	echo "##### Unfiltered #####" >> log_${id}.txt
    elif [[ "${id}" == *_filtered_longest* ]]; then
    	echo "##### Filtered (Longest Isoform)  #####" >> log_${id}.txt
    elif [[ "${id}" == *_filtered_primary* ]]; then
    	echo "##### Filtered (Primary Isoform)  #####" >> log_${id}.txt
    fi

    echo "Total number of genes: \$genes" >> log_${id}.txt
    if [[ "${id}" == *_filtered ]] || [[ "${id}" == *unfiltered* ]]; then
    echo "Total number of transcripts: \$transcripts" >> log_${id}.txt
    fi
    echo "EnTAP alignment rate: \$entap" >> log_${id}.txt
    echo "Mono-exonic/multi-exonic rate: \$mono_multi " >> log_${id}.txt
    echo "BUSCO (\$odb): \$busco" >> log_${id}.txt
    """
}
