process EST_ALIGN {
    publishDir "$params.outdir/03_alignments/est",  mode: 'copy'
    label 'process_medium'

    conda "bioconda::gmap"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/gmap:2021.08.25--pl5321h67092d7_2' :
        'quay.io/biocontainers/gmap:2021.08.25--pl5321h67092d7_2' }"


    input:
    tuple val(id), path(cds)
    path(gmapIndex)
    path(genome)
     
    output:
    tuple val(id), path("${id}.psl"), emit: align
    
    script:
    """
    genome_size=\$(awk '!/^>/ { gsub("[^A-Za-z]", ""); count += length } END { print count }' ${genome})
    if (( \$genome_size < 4294967296 )); then
    	gmap -D ${gmapIndex} -d gmap ${cds} --nthreads=${task.cpus} --min-intronlength=30 --intronlength=500000 --trim-end-exons=20 -f 1 -n 0 > ${id}.psl 
    else
	gmapl -D ${gmapIndex} -d gmap ${cds} --nthreads=${task.cpus} --min-intronlength=30 --intronlength=500000 --trim-end-exons=20 -f 1 -n 0 > ${id}.psl
    fi
    
    """
}
process EST_HINTS {
    publishDir "$params.outdir/05_hints/${id}",  mode: 'copy'
    label 'process_medium'

    container "plantgenomics/easel:perl"

    input:
    tuple val(id), path(gmap)
     
    output:
    tuple val(id), path("${id}.estHints.gff"), emit: est
    
    script:
    """
    cat ${gmap} | sort -n -k 16,16 | sort -s -k 14,14 | perl -ne '@f=split; print if (\$f[0]>=100)' | ${projectDir}/bin/blat2hints.pl --source=E --nomult --ep_cutoff=20 --in=/dev/stdin --out=${id}.estHints.gff
    """
}
process PROTEIN_ALIGN {
    label 'process_high'
    publishDir "$params.outdir/03_alignments/protein",  mode: 'copy'
    
    conda "bioconda::miniprot"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/miniprot:0.11--he4a0461_2' :
        'quay.io/biocontainers/miniprot:0.11--he4a0461_2' }" 

    input:
    path(genome)
    tuple val(id), path(pep)
    val(args)

    output:
    tuple val(id), path("${id}.gtf"), emit: miniprot

    script:
    """
    miniprot -Iut${task.cpus} ${genome} ${pep} --gtf ${args} > ${id}.gtf
    """
}
process PROTEIN_HINTS {
    publishDir "$params.outdir/05_hints/${id}",  mode: 'copy' 
    label 'process_low'
    container 'plantgenomics/easel:perl'

    input:
    tuple val(id), path(miniprot)
    path(genome)

    output:
    tuple val(id), path("${id}.protHints.gff"), emit: protein

    script:
    """
    ${projectDir}/bin/align2hints.pl --in=${miniprot} --out=${id}.protHints.gff --prg=miniprot --genome_file=${genome}
    """
}
process ORTHODB_HINTS {
    publishDir "$params.outdir/05_hints/${id}",  mode: 'copy' 
    label 'process_low'
    container 'plantgenomics/easel:perl'

    input:
    tuple val(id), path(miniprot)
    path(genome)

    output:
    path("${id}.protHints.gff"), emit: protein

    script:
    """
    ${projectDir}/bin/align2hints.pl --in=${miniprot} --out=${id}.protHints.gff --prg=miniprot --genome_file=${genome}
    """
}
process GFFREAD {
    label 'process_low'

    conda "bioconda::gffread=0.12.7"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/gffread:0.12.7--hd03093a_1' :
        'quay.io/biocontainers/gffread:0.12.7--hd03093a_1' }"

    input:
    tuple val(id), path(gmap)
     
    output:
    tuple val(id), path("${id}.gtf"), emit: gtf
    
    script:
    """
    gffread ${gmap} -T -o ${id}.gtf
    """
}
process TRAINING_SET {
    label 'process_high_cpu'

    container "plantgenomics/easel:augustus"
    
    input:
    tuple val(id), path(gtf), path(gff)
    path(genome)
    val(species)
    val(test)
    val(train)
    val(kfold)
    val(rounds)
    path(augustus)

    output:
    tuple val(id), path("${id}.path"), emit: output
     
    script:
  if (params.optimize == true )
  """
  augustus=\$(readlink -f ${augustus}/.easel)
  if [ ! -d \$augustus ]; then
  mkdir \$augustus
  cp -r /usr/share/augustus \$augustus
  fi
  chmod -R 775 \$augustus
  export AUGUSTUS_CONFIG_PATH=\$augustus/augustus/config
  /usr/share/augustus/scripts/computeFlankingRegion.pl ${gtf} | tee calculate_flank.out
  flanking_DNA=\$(grep -A0 "flanking_DNA" calculate_flank.out | awk '{print \$5}')
  echo "\$flanking_DNA" >> calculate_flank.out
  /usr/share/augustus/scripts/gff2gbSmallDNA.pl ${gff} ${genome} \$flanking_DNA ${id}Raw.gb
  if [[ -d \$augustus/augustus/config/species/"$species"_${id} ]]; then rm -rf \$augustus/augustus/config/species/"$species"_${id}; fi
  \$augustus/augustus/scripts/new_species.pl --species="$species"_${id}
  /usr/bin/etraining --species="$species"_${id} ${id}Raw.gb 2> failed_genes.txt
  awk '{print \$7}' failed_genes.txt | sed s/://g > badTrainingGenes.lst
  \$augustus/augustus/scripts/filterGenes.pl badTrainingGenes.lst ${id}Raw.gb > ${id}GoodGenes.gb
  grep -c "LOCUS" *.gb | tee calculate_flank.out
  \$augustus/augustus/scripts/randomSplit.pl ${id}GoodGenes.gb ${test}
  \$augustus/augustus/scripts/randomSplit.pl ${id}GoodGenes.gb.train ${train}
  rm ${id}GoodGenes.gb.train
  rm ${id}GoodGenes.gb.train.train
  mv ${id}GoodGenes.gb.train.test train.gb
  mv ${id}GoodGenes.gb.test test.gb
  grep -c "LOCUS" *.gb >> calculate_flank.out
  /usr/bin/etraining --species="$species"_${id} train.gb
  /usr/bin/augustus --species="$species"_${id} test.gb | tee ${id}_untrained.out
  ls \$augustus/augustus/config/species/"$species"_${id} > ${id}.path

  if [ "${kfold}" -eq 0 ]
  then 
  	\$augustus/augustus/scripts/optimize_augustus.pl --species="$species"_${id} train.gb --cpus=${task.cpus} --kfold=${task.cpus} --rounds=${rounds}
  else
        \$augustus/augustus/scripts/optimize_augustus.pl --species="$species"_${id} train.gb --cpus=${task.cpus} --kfold=${kfold} --rounds=${rounds}	
  fi
  /usr/bin/etraining --species="$species"_${id} train.gb
  /usr/bin/augustus --species="$species"_${id} test.gb | tee ${id}_optimized.out
   """
   else if (params.optimize == false )
   """
  augustus=\$(readlink -f ${augustus}/.easel )
  if [ ! -d \$augustus ]; then
  mkdir \$augustus
  cp -r /usr/share/augustus \$augustus
  fi 
  chmod -R 775 \$augustus
  export AUGUSTUS_CONFIG_PATH=\$augustus/augustus/config
  /usr/share/augustus/scripts/computeFlankingRegion.pl ${gtf} | tee calculate_flank.out
  flanking_DNA=\$(grep -A0 "flanking_DNA" calculate_flank.out | awk '{print \$5}')
  echo "\$flanking_DNA" >> calculate_flank.out
  /usr/share/augustus/scripts/gff2gbSmallDNA.pl ${gff} ${genome} \$flanking_DNA ${id}Raw.gb
  if [[ -d \$augustus/augustus/config/species/"$species"_${id} ]]; then rm -rf \$augustus/augustus/config/species/"$species"_${id}; fi
  \$augustus/augustus/scripts/new_species.pl --species="$species"_${id} 
  /usr/bin/etraining --species="$species"_${id} ${id}Raw.gb 2> failed_genes.txt
  awk '{print \$7}' failed_genes.txt | sed s/://g > badTrainingGenes.lst 
  \$augustus/augustus/scripts/filterGenes.pl badTrainingGenes.lst ${id}Raw.gb > ${id}GoodGenes.gb
  grep -c "LOCUS" *.gb | tee calculate_flank.out
  \$augustus/augustus/scripts/randomSplit.pl ${id}GoodGenes.gb ${test}
  \$augustus/augustus/scripts/randomSplit.pl ${id}GoodGenes.gb.train ${train}
  rm ${id}GoodGenes.gb.train
  rm ${id}GoodGenes.gb.train.train
  mv ${id}GoodGenes.gb.train.test train.gb
  mv ${id}GoodGenes.gb.test test.gb
  grep -c "LOCUS" *.gb >> calculate_flank.out
  /usr/bin/etraining --species="$species"_${id} train.gb
  /usr/bin/augustus --species="$species"_${id} test.gb | tee ${id}_untrained.out
  ls \$augustus/augustus/config/species/"$species"_${id} > ${id}.path
  """
}

process AUGUSTUS_EST {
    label 'process_medium_memory'
    tag { id }

    container "plantgenomics/easel:augustus"

    input:
    path(configEST)
    tuple val(id), path(genome_chunk), val(tool), path(ESThints), path(training)
    val(species)
    path(augustus)
     
    output:
    path("*.ESTHints.gff3"), emit: estHints
     
    script: 
    """
    export AUGUSTUS_CONFIG_PATH=${augustus}/.easel/augustus/config
    /usr/bin/augustus --species="$species"_${tool} ${genome_chunk} --extrinsicCfgFile=${configEST} --alternatives-from-evidence=true --hintsfile=${ESThints} --allow_hinted_splicesites=atac --gff3=on --outfile="$id"_${tool}.ESTHints.gff3
    """
}

process AUGUSTUS_PROTEIN {
    label 'process_medium_memory'
    tag { id }

    container "plantgenomics/easel:augustus"

    input:
    path(configProtein)
    tuple val(id), path(genome_chunk), val(tool), path(proteinHints), path(training)
    val(species)
    path(augustus)
     
    output:
    path("*.protHints.gff3"), emit: proteinHints
    
    script:  
    """
    export AUGUSTUS_CONFIG_PATH=${augustus}/.easel/augustus/config
    /usr/bin/augustus --species="$species"_${tool} ${genome_chunk} --extrinsicCfgFile=${configProtein} --alternatives-from-evidence=true --hintsfile=${proteinHints} --allow_hinted_splicesites=atac --gff3=on --outfile="$id"_${tool}.protHints.gff3

    """
}
process AUGUSTUS_ORTHODB {
    label 'process_medium_memory'
    tag { id }

    container "plantgenomics/easel:augustus"

    input:
    path(configProtein)
    tuple val(id), path(genome_chunk), val(tool), path(proteinHints), path(training)
    val(species)
    path(augustus)
     
    output:
    path("*.protHints.gff3"), emit: orthodb_proteinHints
    
    script:  
    """
    export AUGUSTUS_CONFIG_PATH=${augustus}/.easel/augustus/config
    /usr/bin/augustus --species="$species"_${tool} ${genome_chunk} --extrinsicCfgFile=${configProtein} --alternatives-from-evidence=true --hintsfile=${proteinHints} --allow_hinted_splicesites=atac --gff3=on --outfile="$id"_${tool}.protHints.gff3

    """
}
process COMBINE_EST {
    publishDir "$params.outdir/06_predictions/psiclass", mode: 'copy', pattern: "est_psiclass.gff"
    publishDir "$params.outdir/06_predictions/stringtie2", mode: 'copy', pattern: "est_stringtie2.gff"
    label 'process_medium_memory'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.2.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.2.0--pl5321hdfd78af_0' }"

    input:
    path(estHints)
     
    output:
    path("est_psiclass.gff"), emit: est_psiclass
    path("est_stringtie2.gff"), emit: est_stringtie2
      
    """
mkdir est
mv ${estHints} est
for f in est/*.gff3
do
    if [ \$(wc -l < "\$f") -lt 30 ]; then
        rm \$f 
    else
	    echo "-f \$f" >> files.txt
    fi
done

if grep -q 'stringtie2' files.txt; then
    grep '_stringtie2\\.' files.txt > stringtie2_files.txt
fi
if grep -q 'psiclass' files.txt; then
grep '_psiclass\\.' files.txt > psiclass_files.txt
fi

if [ \$(wc -l < psiclass_files.txt) -eq 1 ]; then
    mv ${estHints} est_psiclass.gff
    sed -i '/^#/d' est_psiclass.gff
else 
    files=\$(sed -n 's/..*/&/p' psiclass_files.txt | paste -sd' ' - | cat)
    gff=\$(echo \$files)
    agat_sp_merge_annotations.pl \$gff -o est_psiclass.gff
    sed -i '/^#/d' est_psiclass.gff
fi

if [ \$(wc -l < stringtie2_files.txt) -eq 1 ]; then
    mv ${estHints} est_stringtie2.gff
    sed -i '/^#/d' est_stringtie2.gff
else 
    files=\$(sed -n 's/..*/&/p' stringtie2_files.txt | paste -sd' ' - | cat)
    gff=\$(echo \$files)
    agat_sp_merge_annotations.pl \$gff -o est_stringtie2.gff
    sed -i '/^#/d' est_stringtie2.gff
fi

    """
}

process COMBINE_PROTEIN {
    publishDir "$params.outdir/06_predictions/psiclass", mode: 'copy', pattern: "protein_psiclass.gff"
    publishDir "$params.outdir/06_predictions/stringtie2", mode: 'copy', pattern: "protein_stringtie2.gff"
    label 'process_medium_memory'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.2.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.2.0--pl5321hdfd78af_0' }"

    input:
    path(proteinHints)
     
    output:
    path("protein_psiclass.gff"), emit: protein_psiclass
    path("protein_stringtie2.gff"), emit: protein_stringtie2
      
    """
mkdir protein
mv ${proteinHints} protein
for f in protein/*.gff3
do
    if [ \$(wc -l < "\$f") -lt 30 ]; then
        rm \$f 
    else
	    echo "-f \$f" >> files.txt
    fi
done

if grep -q 'stringtie2' files.txt; then
    grep '_stringtie2\\.' files.txt > stringtie2_files.txt
fi
if grep -q 'psiclass' files.txt; then
grep '_psiclass\\.' files.txt > psiclass_files.txt
fi

if [ \$(wc -l < psiclass_files.txt) -eq 1 ]; then
    mv ${proteinHints} protein_psiclass.gff
    sed -i '/^#/d' protein_psiclass.gff
else 
    files=\$(sed -n 's/..*/&/p' psiclass_files.txt | paste -sd' ' - | cat)
    gff=\$(echo \$files)
    agat_sp_merge_annotations.pl \$gff -o protein_psiclass.gff
    sed -i '/^#/d' protein_psiclass.gff
fi

if [ \$(wc -l < stringtie2_files.txt) -eq 1 ]; then
    mv ${proteinHints} protein_stringtie2.gff
    sed -i '/^#/d' protein_stringtie2.gff
else 
    files=\$(sed -n 's/..*/&/p' stringtie2_files.txt | paste -sd' ' - | cat)
    gff=\$(echo \$files)
    agat_sp_merge_annotations.pl \$gff -o protein_stringtie2.gff
    sed -i '/^#/d' protein_stringtie2.gff
fi
    """
}

process COMBINE_ORTHODB {
    publishDir "$params.outdir/06_predictions/psiclass", mode: 'copy', pattern: "orthodb_psiclass.gff"
    publishDir "$params.outdir/06_predictions/stringtie2", mode: 'copy', pattern: "orthodb_stringtie2.gff"
    label 'process_medium_memory'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.2.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.2.0--pl5321hdfd78af_0' }"

    input:
    path(proteinHints)
     
    output:
    path("orthodb_psiclass.gff"), emit: orthodb_psiclass
    path("orthodb_stringtie2.gff"), emit: orthodb_stringtie2
      
    """
mkdir protein
mv ${proteinHints} protein
for f in protein/*.gff3
do
    if [ \$(wc -l < "\$f") -lt 30 ]; then
        rm \$f 
    else
        echo "-f \$f" >> files.txt
    fi
done

if grep -q 'stringtie2' files.txt; then
    grep '_stringtie2\\.' files.txt > stringtie2_files.txt
fi
if grep -q 'psiclass' files.txt; then
grep '_psiclass\\.' files.txt > psiclass_files.txt
fi

if [ \$(wc -l < psiclass_files.txt) -eq 1 ]; then
    mv ${proteinHints} orthodb_psiclass.gff
    sed -i '/^#/d' orthodb_psiclass.gff
else 
    files=\$(sed -n 's/..*/&/p' psiclass_files.txt | paste -sd' ' - | cat)
    gff=\$(echo \$files)
    agat_sp_merge_annotations.pl \$gff -o orthodb_psiclass.gff
    sed -i '/^#/d' orthodb_psiclass.gff
fi

if [ \$(wc -l < stringtie2_files.txt) -eq 1 ]; then
    mv ${proteinHints} orthodb_stringtie2.gff
else 
    files=\$(sed -n 's/..*/&/p' stringtie2_files.txt | paste -sd' ' - | cat)
    gff=\$(echo \$files)
    agat_sp_merge_annotations.pl \$gff -o orthodb_stringtie2.gff
fi
    """
}
process PREDICTIONS {
    publishDir "$params.outdir/06_predictions",  mode: 'copy'
    label 'process_single'
    
    input:
    path(gff)

    output:
    path("rna.txt"), emit: rna
    path("augustus.txt"), emit: augustus

    script:

    """
    mkdir links
    mv ${gff} links
    for f in links/*.gff*
    do
        readlink -f \$f >> list.txt
    done

    grep 'transdecoder' list.txt > rna.txt
    grep -E 'orthodb|protein|est' list.txt > augustus.txt

    """
}
process COMBINE_RNA {
    label 'process_medium_memory'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.2.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.2.0--pl5321hdfd78af_0' }"

    input:
    path(gff)
     
    output:
    path("combined_rna.gff"), emit: transdecoder

    script: 

    """
    mkdir combine
    mv ${gff} combine
    for f in combine/*.gff*
    do
        echo "-f \$f" >> combine_files.txt
    done
    files=\$(sed -n 's/..*/&/p' combine_files.txt | paste -sd' ' - | cat)
    gff=\$(echo \$files)
    agat_sp_merge_annotations.pl \$gff -o combined_rna.gff

   """
}
process COMPARE_RNA {
    label 'process_medium'

    conda "bioconda::gffcompare=0.12.6"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/gffcompare:0.12.6--h4ac6f70_2' :
        'quay.io/biocontainers/gffcompare:0.12.6--h4ac6f70_2' }"

    input:
    path(gff)
     
    output:
    path("gffcmp.combined.gtf"), emit: gffcompare

    script: 
    //This works but not with container: awk 'FNR==NR { transcripts[\$1]; next } { if ((match(\$0, /ID=([^;]+);/, m) && m[1] in transcripts) || (match(\$0, /Parent=([^;]+)/, m) && m[1] in transcripts)) print }' transcripts.txt ${gff} > rna.gff

    """
    gffcompare ${gff}

   """
}
process GRAB_TRANSCRIPTS {
    label 'process_medium'

    container 'plantgenomics/easel:python'

    input:
    path(gff)
    path(gffcompare)
     
    output:
    path("rna.gff"), emit: rna

    script: 
    //This works but not with container: 

    """
    awk -F'\\t' '\$9 {split(\$9, a, ";"); for (i in a) {if (match(a[i], "oId")) {gsub(/"/, "", a[i]); split(a[i], b, " "); print b[2]}}}' ${gffcompare} > transcripts.txt 
    python ${projectDir}/bin/grab_transcript.py transcripts.txt ${gff} rna.gff

   """
}
process FIX_EXONS {
    label 'process_medium_memory'

    conda "bioconda::gffread"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/gffread:0.12.7--hd03093a_1' :
        'quay.io/biocontainers/gffread:0.12.7--hd03093a_1' }"

    input:
    tuple val(id), path(gff)
    path(genome)
    
     
    output:
    tuple val(id), path("*fix_exons.gff"), emit: fixed
    
    script: 
    if (params.utr == false)
    """
    awk '\$3 !~ /^(exon|five_prime_UTR|three_prime_UTR)\$/' ${gff} | gffread --force-exons --keep-genes | gffread -Z -J -C -g ${genome} | sed 's/geneID/gene_id/g' > force_exons.gff
    awk -F'\t' '(\$3 == "exon" || \$3 == "CDS") && (\$5 - \$4 + 1) < 3 { print \$9 }' force_exons.gff | awk -F';' '{split(\$1, arr, "="); print arr[2]}' > transcripts.txt
    if [ -s "transcripts.txt" ]; then
        grep -wFf transcripts.txt -v force_exons.gff | gffread -C | sed 's/geneID/gene_id/g' > ${id}_fix_exons.gff
        sed -i 's/\tmRNA\t/\ttranscript\t/g' ${id}_fix_exons.gff
    else
        mv force_exons.gff ${id}_fix_exons.gff 
        sed -i 's/\tmRNA\t/\ttranscript\t/g' ${id}_fix_exons.gff
    fi
    """
    else if (params.utr == true)
    """
    gffread ${gff} --keep-genes -Z -J -C -g ${genome} | sed 's/geneID/gene_id/g' > force_exons.gff
    awk -F'\t' '(\$3 == "exon" || \$3 == "CDS") && (\$5 - \$4 + 1) < 3 { print \$9 }' force_exons.gff | awk -F';' '{split(\$1, arr, "="); print arr[2]}' > transcripts.txt
    if [ -s "transcripts.txt" ]; then
        grep -wFf transcripts.txt -v force_exons.gff | gffread -C | sed 's/geneID/gene_id/g' > ${id}_fix_exons.gff
        sed -i 's/\tmRNA\t/\ttranscript\t/g' ${id}_fix_exons.gff
    else
        mv force_exons.gff ${id}_fix_exons.gff 
        sed -i 's/\tmRNA\t/\ttranscript\t/g' ${id}_fix_exons.gff
    fi
    """

}

process RENAME_ATTRIBUTES {
    label 'process_medium_memory'


    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.2.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.2.0--pl5321hdfd78af_0' }"


    input:
    tuple val(id), path(gff)


    output:
    path("${id}_renamed.gff"), emit: rename

    """

    agat_sp_manage_IDs.pl --gff ${gff} -o ${id}_manageID.gff
    sed -E 's/(ID=|Parent=)([^;]+)/\\1${id}_\\2/g' ${id}_manageID.gff | grep -v '^#' > ${id}_prefix.gff 
    agat_sp_manage_attributes.pl --gff ${id}_prefix.gff --att gene_id -o ${id}_renamed.gff

    """
}
process COMBINE_ALL {
    label 'process_medium_memory'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.2.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.2.0--pl5321hdfd78af_0' }"

    input:
    path(gff)
     
    output:
    path("combined.gff"), emit: combined

    script: 

    """
    mkdir combine
    mv ${gff} combine
    for f in combine/*.gff*
    do
        echo "-f \$f" >> combine_files.txt
    done
    files=\$(sed -n 's/..*/&/p' combine_files.txt | paste -sd' ' - | cat)
    gff=\$(echo \$files)
    agat_sp_merge_annotations.pl \$gff -o combined.gff
    sed -i 's/\ttranscript\t/\tmRNA\t/g' combined.gff

   """
}

process COMPLETE_GENEMODEL {
    label 'process_medium_memory'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.2.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.2.0--pl5321hdfd78af_0' }"


    input:
    path(gff)
    path(genome)
     
    output:
    path("complete_easel.gff"), emit: start
    
    """
    agat_sp_add_start_and_stop.pl --gff ${gff} --fasta ${genome} -o complete_easel.gff

    """
}
process GENE_OVERLAP {
    label 'process_medium_memory'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.2.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.2.0--pl5321hdfd78af_0' }"


    input:
    path(gff)


    output:
    path("remove_duplicates.gff"), emit: overlap_unfiltered

    """
    agat_sp_fix_overlaping_genes.pl --gff ${gff} -o overlap_unfiltered.gff
    agat_sp_fix_features_locations_duplicated.pl --gff overlap_unfiltered.gff -o remove_duplicates.gff

    """
}

process FORMAT_GFF {
    publishDir "$params.outdir/final_predictions",  mode: 'copy'
    label 'process_medium_memory'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.2.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.2.0--pl5321hdfd78af_0' }"

    input:
    path(gff)
    path(genome)
    val(id)
     
    output:
    path("*_unfiltered.gff"), emit: unfiltered_fixed

    //-c ${projectDir}/bin/agat_config.yaml 
    
    """
    agat_sp_manage_IDs.pl --gff ${gff} -o reformat.gff
    agat_sp_fix_features_locations_duplicated.pl --gff reformat.gff -o remove_duplicates.gff
    agat_sp_manage_attributes.pl --gff remove_duplicates.gff --att merged_merged_score,merged_merged_ID,merged_merged_Parent,merged_merged_gene_id,merged_ID,merged_Parent,merged_gene_id,merged_score -o rename.gff
    sed -E 's/(ID=|Parent=)([^;]+)/\\1${id}_\\2/g' rename.gff | grep -v '^#' | awk -F'\\t' '{OFS="\\t"; \$2="EASEL"; print}' > ${id}_unfiltered.gff 
    sed -i 's/\tmRNA\t/\ttranscript\t/g' ${id}_unfiltered.gff

    """
}
process LIST {
    label 'process_single'
    
    input:
    path(gff)

    output:
    path("list.txt"), emit: gff

    script:

    """
    mkdir links
    mv ${gff} links
    for f in links/*.gff*
    do
        readlink -f \$f >> list.txt
    done

    """
}
