process PSICLASS {
    publishDir "$params.outdir/04_assembly/psiclass/gtf",  mode: 'copy', pattern: "*.gtf"
    label 'process_medium'

    container "sagnikbanerjee15/psiclass:1.0.3"
     
    input:
    path(bam)
     
    output: 
    path("*gtf"), emit: all_gtf
    path("psiclass.gtf"), emit: gtf
      
    """
    bam=\$(echo '${bam}' | sed -e "s/ /,/g") 
    psiclass -b \$bam -p ${task.cpus}
    mv psiclass_vote.gtf psiclass.gtf
    """
}

process STRINGTIE2 {
        publishDir "$params.outdir/04_assembly/stringtie2/gtf",  mode: 'copy'   
        label 'process_medium' 
        tag { id }

    conda "bioconda::stringtie=2.2.1"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/stringtie:2.2.1--hecb563c_2' :
        'quay.io/biocontainers/stringtie:2.2.1--hecb563c_2' }"

        input:
        tuple val(id), path(bam)
        val(gap)

        output:
        path("*.gtf"), emit: stringtie2_gtf 

        script:
        """ 
        stringtie ${bam} -p ${task.cpus} -g ${gap} -o ${id}.gtf
        """
}

process MERGE_STRINGTIE2 {
    publishDir "$params.outdir/04_assembly/stringtie2/gtf",  mode: 'copy', pattern: '*gtf'

    label 'process_medium' 

    conda "bioconda::stringtie=2.2.1"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/stringtie:2.2.1--hecb563c_2' :
        'quay.io/biocontainers/stringtie:2.2.1--hecb563c_2' }"

        input:
    file(gtf)
    
        output:
    path("stringtie2.gtf"), emit: merged_gtf

        script:
        """   
    stringtie --merge -p ${task.cpus} -o stringtie2.gtf *.gtf
        """
}
process FASTA {
    publishDir "$params.outdir/04_assembly/${id}",  mode: 'copy', pattern: '*fa'    
    publishDir "$params.outdir/log",  mode: 'copy', pattern: '*txt'
    tag { id }

        
    conda "bioconda::transdecoder"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/transdecoder:5.7.1--pl5321hdfd78af_0' :
        'quay.io/biocontainers/transdecoder:5.7.1--pl5321hdfd78af_0' }"
    label 'process_low' 

        input:
    path(genome)
    tuple val(id), path(gtf)
    
    
        output:
    tuple val(id), path("${id}_transcripts.fa"), emit: fasta
    tuple val(id), path("${id}_transcripts.gff3"), emit: gff3  
    path("log_${id}.txt"), emit: log

        script:
        """   
    ${projectDir}/bin/gtf_genome_to_cdna_fasta.pl ${gtf} ${genome} > ${id}_transcripts.fa 
    ${projectDir}/bin/gtf_to_alignment_gff3.pl ${gtf} > ${id}_transcripts.gff3

    transcripts=\$(echo | grep -c ">" ${id}_transcripts.fa)
    echo " " >> log_${id}.txt

    if [ "${id}" == 'stringtie2' ]; then
        echo "##### StringTie2 Transcriptome Assembly #####" >> log_${id}.txt
    else
        echo "##### PsiCLASS Transcriptome Assembly #####" >> log_${id}.txt
    fi
    echo "\$transcripts transcripts" >> log_${id}.txt
        """
}
process ORF {
    publishDir "$params.outdir/04_assembly/${id}/frameselection",  mode: 'copy'
    label 'process_medium'
    tag { id }

    conda "bioconda::transdecoder"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/transdecoder:5.7.1--pl5321hdfd78af_0' :
        'quay.io/biocontainers/transdecoder:5.7.1--pl5321hdfd78af_0' }"

    input:
    tuple val(id), path(transcriptome) 
     
    output:
    tuple val(id), path("${id}_transcripts.fa.transdecoder_dir/longest_orfs.pep"), emit: longOrfsPepFiles
    tuple val(id), path("${id}_transcripts.fa.transdecoder_dir/"), emit: LongOrfsDirFiles 
    //tuple val(id), path("${id}_transcripts.fa.transdecoder_dir/__checkpoints_longorfs/"), emit: LongOrfsCheckpointsFiles
      
    //rm -r *.transdecoder_dir/__checkpoints_TDpredict/*
    script:
    """
    TransDecoder.LongOrfs -t ${transcriptome}
    rm -r *.transdecoder_dir/__checkpoints_longorfs
    """
}

process EGGNOG {
    publishDir "$params.outdir/04_assembly/${id}/frameselection",  mode: 'copy'
    label 'process_medium' 
    tag { id }

    conda "bioconda::eggnog-mapper"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/eggnog-mapper:2.1.9--pyhdfd78af_0' :
        'quay.io/biocontainers/eggnog-mapper:2.1.9--pyhdfd78af_0' }"

    input:
    tuple val(id), path(protein)  
    path(db)  

    output:
    tuple val(id), path("${id}_eggnog.blastp.emapper.hits"), emit: eggnogBlastp 
   
    script:
    """
    emapper.py -i ${protein} --dmnd_db ${db} --no_annot -o ${id}_eggnog.blastp -m diamond --cpu ${task.cpus}

    """
}

process PREDICT {
    publishDir "$params.outdir/04_assembly/${id}/frameselection",  mode: 'copy'
    label 'process_medium'
    tag { id }
    
    conda "bioconda::transdecoder"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/transdecoder:5.7.1--pl5321hdfd78af_0' :
        'quay.io/biocontainers/transdecoder:5.7.1--pl5321hdfd78af_0' }"

    input:
    tuple val(id), path(transcriptomePredict), path(eggnogBlastp), path(directory)
     
    output:
    tuple val(id), path("${id}_transcripts.fa.transdecoder.pep"), emit: transdecoderPEP
    tuple val(id), path("${id}_transcripts.fa.transdecoder.bed"), emit: transdecoderBED
    tuple val(id), path("${id}_transcripts.fa.transdecoder.cds"), emit: transdecoderCDS
    tuple val(id), path("${id}_transcripts.fa.transdecoder.gff3"), emit: transdecoderGFF3

    script:
    """
    TransDecoder.Predict -t ${transcriptomePredict} --no_refine_starts --retain_blastp_hits ${eggnogBlastp}
    rm -r *.transdecoder_dir/__checkpoints_TDpredict
    """
}
process GENEMODEL {
    publishDir "$params.outdir/04_assembly/${id}",  mode: 'copy'
    label 'process_medium'
    tag { id }

    conda "bioconda::transdecoder"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/transdecoder:5.7.1--pl5321hdfd78af_0' :
        'quay.io/biocontainers/transdecoder:5.7.1--pl5321hdfd78af_0' }"

    input:
    tuple val(id), path(orf), path(gff3), path(transcript)
     
    output:
    tuple val(id), path("${id}.fasta.transdecoder.genome.gff3"), emit: genomeGFF3
    path("psiclass.fasta.transdecoder.genome.gff3"), emit: psiclass_model, optional: true
    path("stringtie2.fasta.transdecoder.genome.gff3"), emit: stringtie2_model, optional: true

    script:
    """
    ${projectDir}/bin/PerlLib/*.pm .
      ${projectDir}/bin/cdna_alignment_orf_to_genome_orf.pl \\
        ${orf} \\
        ${gff3} \\
        ${transcript} > ${id}.fasta.transdecoder.genome.gff3
    """
}
process CLUSTER {
    label 'process_medium'    
    publishDir "$params.outdir/04_assembly/${id}/clustering",  mode: 'copy'
    tag { id }
    
    conda "bioconda::vsearch"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/vsearch:2.22.1--hf1761c0_0' :
        'quay.io/biocontainers/vsearch:2.22.1--hf1761c0_0' }"

    input:
    tuple val(id), path(cds)
    val(clust_id)

    output:
    tuple val(id), path("centroids.cds"), emit: centroids
    path "centroids.uc", emit: uc
 
    script:
    """
    vsearch --notrunclabels --cluster_fast ${cds} --centroids centroids.cds --uc centroids.uc --id ${clust_id} --threads ${task.cpus}
    """

}
  
process GET_COMPLETE {
    publishDir "$params.outdir/04_assembly/${id}/clustering",  mode: 'copy', pattern: '*.cds'
    publishDir "$params.outdir/04_assembly/${id}/clustering",  mode: 'copy', pattern: '*.txt'
    publishDir "$params.outdir/04_assembly/${id}",  mode: 'copy', pattern: '*.gff3'
    label 'process_low'  
    tag { id }

    conda "bioconda::samtools=1.17"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/samtools:1.17--hd87286a_1' :
        'quay.io/biocontainers/samtools:1.17--hd87286a_1' }"


    input:
    tuple val(id), path(centroids), path(gff3)

    output:
    tuple val(id), path("${id}.completeORF.cds"), emit: completeORF
    tuple val(id), path("model.${id}.gff3"), emit: GeneModel
    path("id.txt"), emit: completeOutput
    
    script:
    """
    grep  "type:complete" ${centroids} | awk '{print \$1}' > id.txt 
sed -i -e 's/>//g' id.txt
while read line;
    do samtools faidx ${centroids} \$line >> ${id}.completeORF.cds;
done < id.txt
grep -Fwf id.txt ${gff3} | sed 's/\tmRNA\t/\ttranscript\t/g' > model.${id}.gff3
    """
}
