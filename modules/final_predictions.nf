process GFF {
    label 'process_medium'
    tag { id }
    publishDir "$params.outdir/final_predictions",  mode: 'copy'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.2.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.2.0--pl5321hdfd78af_0' }"

    input:
    tuple val (id), path(gtf)
    val(prefix)

    output: 
    path("${id}.gff"), emit: gff
    path("*_filtered.gff"), emit: filtered_gff, optional: true
    path("*filtered_longest_isoform.gff"), emit: longest_gff, optional: true
    path("*filtered_primary_isoform.gff"), emit: primary_gff, optional: true
      
    """
    agat_convert_sp_gxf2gxf.pl --gff ${gtf} -o convert.gff
    awk -F'\\t' 'BEGIN {OFS = "\\t"} \$3 == "gene" {gsub(/;transcript_id=[^;]+/, ""); print} \$3 != "gene" {print}' convert.gff > ${id}.gff

    """
}
process NUCLEOTIDE {
    publishDir "$params.outdir/final_predictions",  mode: 'copy', pattern: "*.cds"
    tag { id }
    label 'process_low'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.2.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.2.0--pl5321hdfd78af_0' }"
    
    
    input:
    path(genome)
    tuple val (id), path(filtered_prediction)
    val(prefix)
     
    output: 
    path("*.cds"), emit: cds
      
    """
    agat_sp_extract_sequences.pl -g ${filtered_prediction} -f ${genome} -t cds -o ${id}.cds

    """
}
process PROTEIN {
    publishDir "$params.outdir/final_predictions",  mode: 'copy', pattern: "*.pep"
    label 'process_low'
    tag { id }

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.2.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.2.0--pl5321hdfd78af_0' }"
    
    
    input:
    path(genome)
    tuple val(id), path(filtered_prediction)
    val(prefix)
     
    output: 
    path("*.pep"), emit: protein
    path("*_filtered.pep"), emit: filtered, optional: true
    path("*filtered_longest_isoform.pep"), emit: longest, optional: true
    path("*filtered_primary_isoform.pep"), emit: primary, optional: true
      
    """
    agat_sp_extract_sequences.pl -g ${filtered_prediction} -f ${genome} --protein -o ${id}.pep

    """
}
