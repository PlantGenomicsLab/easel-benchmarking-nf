process START_STOP_CODONS {
    label 'process_medium'
    container 'plantgenomics/easel:python'
    
    input:
    path(unfiltered_prediction)
    path(matrix)
     
    output: 
    path("start.tracking"), emit: start_codon
    path("stop.tracking"), emit: stop_codon
      
    """
grep -P "\tstart_codon\t" ${unfiltered_prediction} | cut -f9 | sed 's/.*Parent=\\([^;]*\\).*/\\1/' | uniq -c | awk '{print \$2 "\t" \$1}' > start_to_transcript.txt
grep -P "\tstop_codon\t" ${unfiltered_prediction} | cut -f9 | sed 's/.*Parent=\\([^;]*\\).*/\\1/' | uniq -c | awk '{print \$2 "\t" \$1}' > stop_to_transcript.txt      
sed -i \$'1 i\\\nTranscript\tHit' start_to_transcript.txt
sed -i \$'1 i\\\nTranscript\tHit' stop_to_transcript.txt
python ${projectDir}/bin/map_transcript.py ${matrix} start_to_transcript.txt Start_Codon feature1.tracking
python ${projectDir}/bin/map_transcript.py ${matrix} stop_to_transcript.txt Stop_Codon feature2.tracking
awk 'BEGIN { FS=OFS="\t" } NR > 1 { \$4 = sprintf("%d", \$4) }; 1' OFS="\t" feature1.tracking > start.tracking
awk 'BEGIN { FS=OFS="\t" } NR > 1 { \$4 = sprintf("%d", \$4) }; 1' OFS="\t" feature2.tracking > stop.tracking
    """
}