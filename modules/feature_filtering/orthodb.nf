process ORTHODB_BED {
    label 'process_medium'

    conda "bioconda::bedtools"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/bedtools:2.30.0--hc088bd4_0' :
        'quay.io/biocontainers/bedtools:2.30.0--hc088bd4_0' }"


    input:
    path(orthodb_gff)
    path(unfiltered_gff)
    val(f)
    val(F)

    output:
    path "support.txt", emit: bed

    script:
    """
awk '\$3 == "transcript"' ${orthodb_gff} > miniprot_transcript.gff
awk '\$3 == "transcript"' ${unfiltered_gff} > augustus_transcript.gff
bedtools intersect -a augustus_transcript.gff -b miniprot_transcript.gff -s -f ${f} -F ${F} -c | awk '{print \$9, \$10}' | awk -F'[=; ]' -v OFS='\t' '{print \$2,\$5}' > support.txt
sed -i \$'1 i\\\nTranscript\tHit' support.txt

"""
}
process ORTHODB_FEATURE {
    label 'process_low'
    container 'plantgenomics/easel:python'

    input:
    path(matrix)
    path(orthodb)

    output:
    path "orthodb.tracking", emit: orthodb

    script:
    """
python ${projectDir}/bin/map_transcript.py ${matrix} ${orthodb} OrthoDB orthodb.tracking

"""
}
