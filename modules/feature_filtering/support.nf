process SUPPORT {
    label 'process_medium'

    conda "bioconda::bedtools"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/bedtools:2.30.0--hc088bd4_0' :
        'quay.io/biocontainers/bedtools:2.30.0--hc088bd4_0' }"
     
    input:
    path(unfiltered_prediction)
    path(list)
     
    output: 
    path("support.tracking"), emit: support
      
    """
awk '\$3 == "transcript"' ${unfiltered_prediction} > transcript.gff
mkdir filtered

cat ${list} | while read path
do
    awk '\$3 == "transcript" || \$3 == "mRNA"' "\$path" >> "\$path".transcript.gff
    mv "\$path".transcript.gff filtered/
done

gff=\$(echo filtered/*.gff)
bedtools intersect -a transcript.gff -b \$gff -s -f 1.0 -F 1.0 -c | awk '{print \$9, \$10}' | awk -F'[=; ]' -v OFS='\t' '{print \$4,\$2,\$5}' > support.tracking
sed -i \$'1 i\\\nGene\tTranscript\tSupport' support.tracking

    """
}

