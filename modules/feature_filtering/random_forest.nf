process REGRESSOR {
    label 'process_medium'
    publishDir "$params.outdir/07_filtering",  mode: 'copy' 
    container 'plantgenomics/easel:python'
    
    input:
    path(test)
    path(train)
     
    output: 
    path("regressor_prediction.csv"), emit: r_rf
      
    """
    python ${projectDir}/bin/random_forest_regressor_predict.py ${train} ${test} regressor.csv
    if awk '\$12 == "" {exit 1}' features.tracking; then
  	echo "features.tracking does not contain empty cells."
        mv regressor.csv regressor_prediction.csv
    else
        python ${projectDir}/bin/random_forest_regressor_predict_na.py ${train} ${test} na.csv
        cat regressor.csv na.csv > regressor_prediction.csv
    fi
    """
}
process CLASSIFIER {
    label 'process_medium'
    publishDir "$params.outdir/07_filtering",  mode: 'copy' 
    container 'plantgenomics/easel:python'
    
    input:
    path(test)
    path(train)
     
    output: 
    path("classifier_prediction.csv"), emit: c_rf
      
    """
    python ${projectDir}/bin/random_forest_classifier_predict.py ${train} ${test} classifier_prediction.csv

    """
}
process FILTERED_GTF {
    label 'process_low'
    publishDir "$params.outdir/final_predictions",  mode: 'copy' 

    input:
    path(classifier)
    path(regressor)
    path(unfiltered_gtf)
    val(prefix)
    val(reg_val)
     
    output: 
    path("*filtered.gtf"), emit: filtered_prediction
      
    """
tail -n +2 ${classifier} | awk -F"," '\$2==1' > filtered.csv 
tail -n +2 ${regressor} | awk -F"," '\$2>${reg_val}' >> filtered.csv 
awk -F"," '{ print \$1 }' filtered.csv | sort -u > transcripts.txt

awk 'FNR==NR { transcripts[\$1]; next } { match(\$0, /transcript_id "([^"]+)";/, m); if (m[1] in transcripts) print }' transcripts.txt ${unfiltered_gtf} > ${prefix}_filtered.gtf

    """
}
process FILTERED_GFF {
    label 'process_medium'
    publishDir "$params.outdir/final_predictions",  mode: 'copy' 
    
    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.2.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.2.0--pl5321hdfd78af_0' }"

    input:
    path(gtf)
    val(prefix)

    output: 
    path("*filtered.gff"), emit: filtered_gff
      
    """

agat_convert_sp_gxf2gxf.pl --gff ${gtf} --out ${prefix}_filtered.gff

    """
}
