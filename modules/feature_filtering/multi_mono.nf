process MONO_MULTI {
    label 'process_low'
    container 'plantgenomics/easel:python'
    
    input:
    path(unfiltered_prediction)
    path(matrix)
     
    output: 
    path("multi_mono.tracking"), emit: exonic
      
    """
grep -P "\texon\t" ${unfiltered_prediction} | cut -f9 | sed 's/.*Parent=\\([^;]*\\).*/\\1/' | uniq -c | awk '{print \$2 "\t" \$1}' > exon_to_transcript.txt    
sed -i \$'1 i\\\nTranscript\tHit' exon_to_transcript.txt
python ${projectDir}/bin/map_transcript.py ${matrix} exon_to_transcript.txt Exons feature.tracking
awk 'BEGIN { FS=OFS="\t" } NR > 1 { \$4 = sprintf("%d", \$4) }; 1' OFS="\t" feature.tracking > multi_mono.tracking
    """
}
