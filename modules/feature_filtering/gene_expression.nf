process FEATURE_COUNTS {
    label 'process_medium'

    conda "bioconda::subread"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/subread:2.0.6--he4a0461_0' :
        'quay.io/biocontainers/subread:2.0.6--he4a0461_0' }"

    input:
    tuple val(id), path(bam)
    path(unfiltered_gtf)

    output:
    path "*.counts", emit: counts

    script:
    """
featureCounts -T ${task.cpus} -p --countReadPairs -t exon -g transcript_id -a ${unfiltered_gtf} -o ${id}.txt ${bam}
awk '!/^#/ { print \$1, \$7 }' ${id}.txt | tail -n +2 > ${id}.counts
    """
}
process GENE_EXPRESSION {
    label 'process_low'
    container 'plantgenomics/easel:python'

    input:
    path(counts)
    path(matrix)

    output:
    path "expression.tracking", emit: expression

    script:
    """
mkdir read_counts
mv ${counts} read_counts
awk 'NF > 1{ a[\$1] = a[\$1]"\\t"\$2} END {for( I in a ) print I a[I]}' read_counts/*.counts | awk '{sum=0; for (i=2; i<=NF; i++) sum += \$i; print \$1 "\t" sum}' > merged.counts
sed -i \$'1 i\\\nTranscript\tHit\' merged.counts
python ${projectDir}/bin/map_transcript.py ${matrix} merged.counts Expression expression.tracking

    """
}
