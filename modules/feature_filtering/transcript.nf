process TRANSCRIPT_EXTRACT {
    label 'process_low'

    conda "bioconda::agat"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/agat:1.2.0--pl5321hdfd78af_0' :
        'quay.io/biocontainers/agat:1.2.0--pl5321hdfd78af_0' }"
    

    input:
    path(genome)
    path(unfiltered_prediction)
    path(matrix)
    val(parts)

     
    output: 
    path("*.fa"), emit: transcript_part
    path("transcript.fasta"), emit: transcript
      
    """
    awk '{ print \$2 }' ${matrix} > transcript_list.txt
    agat_sp_extract_sequences.pl -g ${unfiltered_prediction} -f ${genome} -t transcript -o transcript_part.fa
    ${projectDir}/bin/fasta-splitter.pl --n-parts ${parts} transcript_part.fa
    mv transcript_part.fa transcript.fasta

    """
}

process LENGTH {
    label 'process_medium'
    container 'plantgenomics/easel:python'
    tag {id}
     
    input:
    tuple val(id), path(transcript)
     
    output: 
    path("*length.txt"), emit: length_txt
      
    """
    python ${projectDir}/bin/add_percentage_masked_cds.py ${transcript}
    
    awk '{ print \$1, \$3 }' OFS="\t" masked.tracking > ${id}_length.txt
   

    """
}

process MOLECULAR_WEIGHT {
    label 'process_medium'
    container 'plantgenomics/easel:python'
    tag {id}
     
    input:
    tuple val(id), path(transcript)
     
    output: 
    path("*weight.txt"), emit: weight_txt
      
    """
    python ${projectDir}/bin/add_molecular_weight.py ${transcript}
    
    awk '{ print \$1, \$4 }' OFS="\t" masked.tracking > ${id}_weight.txt

    """
}

process MERGE_TRANSCRIPT {
    label 'process_low'
    container 'plantgenomics/easel:python'
     
    input:
    path(molecular_weight)
    path(length)
    path(matrix)
     
    output: 
    path("transcript_weight.tracking"), emit: weight
    path("transcript_length.tracking"), emit: length
      
    """
    cat ${molecular_weight} > merge_weight.txt
    sed -i \$'1 i\\\nTranscript\tHit' merge_weight.txt
    python ${projectDir}/bin/map_transcript.py ${matrix} merge_weight.txt Molecular_Weight transcript_weight.tracking

    cat ${length} > merge_length.txt
    sed -i \$'1 i\\\nTranscript\tHit' merge_length.txt
    python ${projectDir}/bin/map_transcript.py ${matrix} merge_length.txt Length transcript_length.tracking

    """
}

process GC {
    label 'process_low'

    conda "bioconda::seqkit"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/seqkit:2.3.1--h9ee0642_0' :
        'quay.io/biocontainers/seqkit:2.3.1--h9ee0642_0' }"
    
    input:
    path(transcript)
     
    output: 
    path("gc_content.txt"), emit: gc
      
    """
    seqkit fx2tab --name --gc ${transcript} > gc.txt 
    awk '{ print \$1, \$5 }' OFS="\t" gc.txt > gc_content.txt
    sed -i \$'1 i\\\nTranscript\tHit' gc_content.txt
    rm ${transcript}

    """
}
process GC_PYTHON {
    label 'process_low'
    container 'plantgenomics/easel:python'
     
    input:
    path(matrix)
    path(gc)
     
    output: 
    path("gc_content.tracking"), emit: gc_track
      
    """
    python ${projectDir}/bin/map_transcript.py ${matrix} ${gc} GC_Content gc_content.tracking

    """
}
