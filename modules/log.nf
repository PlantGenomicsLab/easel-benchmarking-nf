process EASEL {
    label 'process_single'
    publishDir "$params.outdir",  mode: 'copy' 

    input:
    path(genome)
    path(rna_reads)
    path(alignments)
    path(assembly)
    path(annotation)
    path(contam)

    output:
    path("easel_summary.txt"), emit: log
    
    script:
    """
    cat ${genome} ${rna_reads} ${alignments} ${assembly} ${annotation} ${contam} > easel_summary.txt

    """
}
