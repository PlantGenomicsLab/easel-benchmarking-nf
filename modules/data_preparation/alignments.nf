process ORTHODB_ALIGN {
    label 'process_high'
    publishDir "$params.outdir/03_alignments/protein",  mode: 'copy'

    conda "bioconda::miniprot"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/miniprot:0.11--he4a0461_2' :
        'quay.io/biocontainers/miniprot:0.11--he4a0461_2' }"

    input:
    path(genome)
    path(orthodb)
    val(args)

    output:
    path "orthodb.gtf", emit: orthodb_alignment

    script:

"""
miniprot -t ${task.cpus} -d genome.mpi ${genome}
miniprot -Iut${task.cpus} genome.mpi ${orthodb} ${args} --gtf > orthodb.gtf

"""

}
process GMAP_INDEX {
	publishDir "$params.outdir/02_index",  mode: 'copy'
	label 'process_medium'
	
    conda "bioconda::gmap"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/gmap:2021.08.25--pl5321h67092d7_2' :
        'quay.io/biocontainers/gmap:2021.08.25--pl5321h67092d7_2' }"

    	input:
    	path(genome)

    	output:
    	path "gmap/*" , emit: gmapindex
    

    	script:
    	"""
	gmap_build -t ${task.cpus} -D gmap -d gmap ${genome}

    	"""
}
process HISAT2_INDEX {
	publishDir "$params.outdir/02_index/hisat2",  mode: 'copy'
	label 'process_medium'

	conda "bioconda::hisat2=2.2.1"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/hisat2:2.2.1--py38he1b5a44_0' :
        'quay.io/biocontainers/hisat2:2.2.1--py38he1b5a44_0' }"

    	input:
    	path(genome)

    	output:
    	path "hisat2.index" , emit: hisat2index
    

    	script:
    	"""
    	hisat2-build -p ${task.cpus} -f ${genome} hisat2
		mkdir hisat2.index
		mv *.ht* hisat2.index
    	"""
}
process HISAT2_ALIGN {
	publishDir "$params.outdir/03_alignments/mapping_rates",  mode: 'copy', pattern: "*.txt"
	tag { id }
	label 'process_medium'	

	conda "bioconda::hisat2=2.2.1"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/hisat2:2.2.1--py38he1b5a44_0' :
        'quay.io/biocontainers/hisat2:2.2.1--py38he1b5a44_0' }"

    	input:
    	tuple val(id), path(trimmed)
		path(index)
		val(min)
		val(max)
		val(args)

    	output:
    	tuple val(id), path("*.sam"), emit: sam
		tuple val(id), path("*_mapping_rate.txt"), path("*.sam"), emit: sam_tuple
    

    	script:
    	""" 
        hisat2 --dta -q -x ${index}/hisat2 -1 ${trimmed[0]} -2 ${trimmed[1]} -S ${id}.sam --min-intronlen ${min} --max-intronlen ${max} --summary-file ${id}_mapping_rate.txt -p ${task.cpus} ${args}
    	"""
}
process MAPPING_RATE {
	publishDir "$params.outdir/03_alignments/mapping_rates",  mode: 'copy', pattern: "*.txt"
	label 'process_medium'
	tag {id}

	conda "bioconda::samtools=1.17"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/samtools:1.17--hd87286a_1' :
        'quay.io/biocontainers/samtools:1.17--hd87286a_1' }"


    	input:
    	tuple val(id), path(bam)

    	output:
    	tuple val(id), path("*.txt"), path("*.bam"), emit: bam_user
		path("*.txt"), emit: mr
    
    	script:
   		"""
    	samtools flagstat ${bam} -@ ${task.cpus} > ${id}.txt
    	mv ${bam} ${id}_user.bam
    	"""
}
process ALIGNMENT_REMOVE_SAMPLES {
	publishDir "$params.outdir/03_alignments/bam",  mode: 'copy', pattern: '*.bam'
	label 'process_medium'
	tag {id}

	conda "bioconda::samtools=1.17"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/samtools:1.17--hd87286a_1' :
        'quay.io/biocontainers/samtools:1.17--hd87286a_1' }"


    	input:
    	tuple val(id), path(mapping_rate), path(sam)
		val(rate)

    	output:
    	tuple val(id), path("*.bam"), optional:true, emit: pass_bam_tuple
		path("*.bam"), optional:true, emit: pass_bam
		path("*mr.txt"), emit: mr
    
    	script:

    	""" 
    	if [ "${mapping_rate}" == "${id}_mapping_rate.txt" ]; then
			rate=\$(awk '/overall alignment rate/ {gsub(/%/,"",\$1); print \$1}' ${mapping_rate})
			rate_var=\$(echo \$rate)

			if [[ "\${rate_var}" < "${rate}" ]]
			then
        		echo "${id}'s mapping rate is less than ${rate}% and has been removed" > ${id}_mr.txt
			else
				samtools view -b -@ ${task.cpus} ${sam} | samtools sort -o sorted_${id}.bam -@ ${task.cpus}
				echo "${id} \$rate_var" > ${id}_mr.txt
			fi 

			rm *.sam

		elif [ "${mapping_rate}" == "${id}.txt" ]; then 
			rate=\$(grep -m 1 'mapped' ${mapping_rate} | awk '{print \$5}' | cut -d'%' -f1 | cut -c2-)
			rate_var=\$(echo \$rate)

			if [[ "\${rate_var}" < "${rate}" ]]
		then
        	echo "${id}'s mapping rate is less than ${rate}% and has been removed" > ${id}_mr.txt
		else
			samtools sort ${sam} -o sorted_${id}.bam -@ ${task.cpus}
			echo "${id} \$rate_var" > ${id}_mr.txt
			fi 
		fi

    	"""
}
process ALIGNMENT_LOG {
	publishDir "$params.outdir/log",  mode: 'copy'
	label 'process_single'
   
    input:
	path(mr)
	val(rate)

	output:
	path("log_align.txt"), emit: log_align

    script:
    """ 
	header="library rate"
	echo "" > log_align.txt
	echo "##### Alignment Rates #####" >> log_align.txt

	mkdir bam
	mkdir removed
	mkdir kept
	mv ${mr} bam

	for f in bam/*
		do
			if grep -q 'and has been removed' \$f
		then
    		mv \$f removed
		else
			mv \$f kept
		fi
	done

	if [ -z "\$(ls -A kept)" ]; then
		echo "ERROR: All libraries have a mapping rate less than ${rate}% and have been removed. Check that you are using the correct genome and RNA for your species. Reduce cut off with --rate <int> and -resume."
    	exit 1
	else
    	cat kept/* > kept.txt
		echo "Passed:" >> log_align.txt
		echo "\$header" >> log_align.txt
		cat kept.txt | while read line; do echo "\$line"; done >> log_align.txt
	fi

	if [ -z "\$(ls -A removed)" ]; then
		echo "" >> log_align.txt
		echo "Failed:" >> log_align.txt
    	echo "None." >> log_align.txt
		echo "All libraries have a mapping rate greater than ${rate}%" >> log_align.txt
		else
    	cat removed/* > removed.txt
		echo "" >> log_align.txt
		echo "Failed:" >> log_align.txt
		cat removed.txt | while read line; do echo "\$line"; done >> log_align.txt
	fi
    """
}
