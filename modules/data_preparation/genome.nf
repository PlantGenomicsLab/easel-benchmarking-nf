process PERCENT_MASKED {
    label 'process_low'
    publishDir "$params.outdir/log",  mode: 'copy'

    input:
    path(genome)

    output:
    path("log_genome.txt"), emit: log_genome
      
    """
    
masked_bases=\$(awk '!/^>/ { gsub("[^a-z]", ""); count += length } END { print count }' ${genome})
genome_length=\$(awk '!/^>/ { gsub("[^A-Za-z]", ""); count += length } END { print count }' ${genome})
masked_percentage=\$(echo "(\$masked_bases/\$genome_length)*100" | bc -l | xargs printf "%.2f")
high=\$(echo "25")
low=\$(echo "${params.percent_masked}")

if [[ "\$masked_percentage" > "\$low" && "\$masked_percentage" < "\$high" ]]
then
echo "##### Genome #####" >> log_genome.txt
echo "\$masked_percentage % of ${genome} is masked" >> log_genome.txt
echo "WARNING: Less than 25% masked" >> log_genome.txt
elif [[ "\$masked_percentage" < "\$low" ]]
then
echo "######## Genome ########" >> log_genome.txt
echo "\$masked_percentage % of ${genome} is masked" >> log_genome.txt
echo "ERROR: Less than 10% of ${genome} is masked" >> log_genome.txt
exit 1
else
echo "######## Genome ########" >> log_genome.txt
echo "\$masked_percentage % of ${genome} is masked" >> log_genome.txt
fi
    """
}
process SPLIT {
    label 'process_single'
    container 'plantgenomics/easel:perl'
    
    input:
    path(genome)
    val(bins)

    output:
    path("*.part-*"), emit: chromosomes
      
    """
    if grep -q "^[acgt]" ${genome}
    then
    ${projectDir}/bin/fasta-splitter.pl --n-parts ${bins} ${genome}
    else
    echo "ERROR: ${genome} is not masked"
    exit 1
    fi
    """
} 
process FOLD {
    label 'process_single'
    
    conda "bioconda::seqtk"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/seqtk:1.3--h7132678_4' :
        'quay.io/biocontainers/seqtk:1.3--h7132678_4' }"

    input:
    path(genome)
    val(id)

    output:
    path("*.fasta"), emit: fold
      
    """
    genome_filename=\$(basename "${genome}")
    if [ "\$genome_filename" == "${id}_folded.fasta" ]; then
        echo "Genome filename matches "$id"_folded.fasta, please rename"
    else
        seqtk seq -Cl60 ${genome} > "$id"_folded.fasta
    fi
    """
} 

